function button_loading(btn) {
    btn.data('original-text', btn.html()).html(btn.data('loading-text')).prop('disabled', true);
}

function button_reset(btn) {
    btn.html(btn.data('original-text')).prop('disabled', false);
}

export function selectpicker(sel) {
    $(".selectpicker", sel).selectpicker();
}

export function showError(e) {
    if (e.status === 403) {
        alert("Операция запрещена");
    } else {
        alert(e.responseText);
    }
}

export function processResponse(response, options) {
    options = options || {};
    if ("form" in response) {
        makeModal(response.form, options, response);
    } else if ("model" in response) {
        if ("afterClose" in options) {
            options.afterClose(options, response);
        }
    } else {
        showError({responseText: "Invalid response\n" + response, status: 0});
    }
}

export function showModal(_modal, options) {
    const object = {
        type: "GET",
        url: _modal.href,
        dataType: "json"
    };
    $.ajax(object)
        .done(r => processResponse(r, options))
        .fail(e => showError(e));
    return false;
}

export function makeModal(_modal, options, result) {
    _modal = $(_modal);
    options = options || {};
    if ("beforeShow" in options) {
        options.beforeShow(_modal, options, result);
    }
    _modal.modal();
    _modal.on("shown.bs.modal", () => {
        $("body").addClass("modal-open");
        _modal.find("input:visible:first").focus();
    });
    _modal.on("hidden.bs.modal", () => _modal.remove())
    _modal.on("click", ".js-submit", () => submitForm(_modal, options));
}

export function initModal(selector, options) {
    $(selector).on("click", e => showModal(e.currentTarget, options));
}

export function submitForm(_modal, options) {
    const btn = $(".js-submit", _modal);
    const form = $("form", _modal);
    btn.attr("data-loading-text", btn.text() + " ...");
    button_loading(btn);
    const object = {
        type: form.attr('method'),
        url: form.attr('action'),
        data: form.serialize(),
        dataType: "json"
    };
    $.ajax(object)
        .always(() => {
            button_reset(btn);
            _modal.modal("hide");
        })
        .done(r => processResponse(r, options))
        .fail(e => showError(e));
    return false;
}

export function getCsrfParam() {
    return $("meta[name=csrf-param]").attr("content");
}

export function getCsrfToken() {
    return $("meta[name=csrf-token]").attr("content");
}
