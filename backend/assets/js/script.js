import "popper.js/src/index";
import "bootstrap/js/src/modal";
import "bootstrap/js/src/dropdown";
import "bootstrap/js/src/button";
import "bootstrap/js/src/tab";
import "bootstrap-select/js/bootstrap-select";
import HotelImages from "./HotelImages.svelte";
import HotelPhones from "./HotelPhones.svelte";
import {getCsrfParam, getCsrfToken, initModal, selectpicker, showError} from "./utils";

const noImage = "../img/no_image.png";

export function ServiceList() {
    initModal(".js-show-modal", {
        beforeShow: _modal => selectpicker(_modal),
        afterClose: () => window.location.reload(),
    });
}

export function HotelDetail(options) {
    new HotelPhones({
        target: document.querySelector("#js-phone-list"),
        props: {phones: options.phones || [], formName: options.formName || ""}
    });
}

export {
    ServiceList as ServiceGroupList,
    ServiceList as CityList,
    ServiceList as CountryList,
    ServiceList as HotelList,
    HotelDetail as HotelCreate,
};

export function HotelRoom() {
    initModal(".js-show-room-modal", {
        beforeShow: _modal => {
            selectpicker(_modal);
            inputImage(_modal.find("#js-room-image"));
        },
        afterClose: () => window.location.reload(),
    });
}

export function HotelImage(options) {
    new HotelImages({
        target: document.querySelector("#js-image-list"),
        props: {images: options.images || [], formName: options.formName || ""}
    });
}

export function VehicleList() {
    initModal(".js-show-modal", {
        beforeShow: _modal => {
            selectpicker(_modal);
            const updateModelSelectPicker = r => _modal.find(".js-model.selectpicker").html(r).selectpicker("refresh");
            _modal.on("change", ".js-mark.selectpicker", e => {
                $.ajax({
                    url: "/model/list",
                    data: {mark_id: e.currentTarget.value},
                }).done(r => updateModelSelectPicker(r));
            });
        },
        afterClose: () => window.location.reload(),
    });
}

function inputImage(selector) {
    const hiddenInput = selector.find("input");
    const imageBlock = selector.find("img");
    const uploadButton = selector.find(".js-upload");
    const cleanButton = selector.find(".js-clean");
    const errorMessage = selector.find(".invalid-feedback");

    cleanButton.on("click", () => {
        imageBlock.attr("src", noImage);
        errorMessage.html("");
        hiddenInput.val("");
    });

    uploadButton.on("click", () => {
        const fileInput = $("<input type=\"file\" accept=\"image/*\">");
        fileInput.on("change", e => {
            const formData = new FormData();
            formData.append("file", e.currentTarget.files[0]);
            formData.append(getCsrfParam(), getCsrfToken());
            $.ajax({
                url: "/site/upload",
                type: "post",
                contentType: false,
                processData: false,
                data: formData,
            }).done(r => {
                if (r.success) {
                    imageBlock.attr("src", r.model.url);
                    hiddenInput.val(r.model.id);
                    errorMessage.html("");
                } else {
                    imageBlock.attr("src", noImage);
                    errorMessage.html("Ошибка загрузки файла");
                    hiddenInput.val("");
                }
            }).fail(e => showError(e)).always(() => fileInput.remove());
        }).click();
    });
}

export function initHotelMap() {
    let inputLat = $("#js-lat"),
        inputLng = $("#js-lng"),
        lat = inputLat.val(),
        lng = inputLng.val();
    let pos = {
        lat: parseFloat(lat || 55.7522200),
        lng: parseFloat(lng || 37.6155600)
    };
    let map = new google.maps.Map(document.getElementById("js-maps"), {
        zoom: 6,
        center: pos,
        disableDefaultUI: true
    });
    let marker = new google.maps.Marker({
        position: pos,
        map: map,
        draggable: true,
        title: "Укажите местоположение объекта"
    });
    marker.addListener("dragend", function (e) {
        inputLat.val(e.latLng.lat());
        inputLng.val(e.latLng.lng());
    });
    marker.setPosition(pos);
    map.panTo(pos);
    map.setZoom(16);
}