<?php

namespace backend\models;

use Yii;
use yii\helpers\ArrayHelper;

class Service extends \common\models\Service
{
    public static function getList(): array
    {
        return ArrayHelper::map(
            static::find()->andWhere(['status' => self::STATUS_ACTIVE])->with('serviceGroup')->all(),
            'id',
            'name',
            'serviceGroup.name'
        );
    }

    public function rules(): array
    {
        return [
            ['name', 'trim'],
            [['service_group_id', 'name'], 'required'],
            ['service_group_id', 'exist', 'targetRelation' => 'serviceGroup'],
            ['name', 'string', 'max' => 255],
            ['name', 'unique'],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => array_keys(static::getStatusList())],
        ];
    }

    public function getServiceGroup()
    {
        return $this->hasOne(ServiceGroup::class, ['id' => 'service_group_id']);
    }

    public function attributeLabels(): array
    {
        return array_merge(
            parent::attributeLabels(),
            [
                'serviceGroup.name' => Yii::t('app', 'Service group'),
            ]
        );
    }
}