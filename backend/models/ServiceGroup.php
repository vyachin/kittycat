<?php

namespace backend\models;

class ServiceGroup extends \common\models\ServiceGroup
{
    public static function getList(): array
    {
        return static::find()
            ->select(['name', 'id'])
            ->andWhere(['status' => self::STATUS_ACTIVE])
            ->orderBy(['name' => SORT_ASC])
            ->indexBy('id')
            ->column();
    }

    public function rules(): array
    {
        return [
            ['name', 'trim'],
            [['name'], 'required'],
            ['name', 'string', 'max' => 255],
            ['name', 'unique'],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => array_keys(static::getStatusList())],
        ];
    }
}