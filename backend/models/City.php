<?php

namespace backend\models;

class City extends \common\models\City
{
    public static function getList($countryId): array
    {
        return static::find()
            ->select(['name', 'id'])
            ->andWhere(['status' => self::STATUS_ACTIVE, 'country_id' => $countryId])
            ->orderBy(['name' => SORT_ASC])
            ->indexBy('id')
            ->column();
    }

    public function rules(): array
    {
        return [
            ['name', 'trim'],
            [['country_id', 'name'], 'required'],
            ['country_id', 'exist', 'targetRelation' => 'country'],
            ['name', 'string', 'max' => 255],
            ['name', 'unique', 'targetAttribute' => ['country_id', 'name']],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => array_keys(static::getStatusList())],
        ];
    }
}