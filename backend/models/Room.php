<?php

namespace backend\models;

use Yii;

class Room extends \common\models\Room
{
    public function rules(): array
    {
        return [
            ['name', 'trim'],
            [['name', 'hotel_id', 'price', 'max_pets'], 'required'],
            ['hotel_id', 'exist', 'targetRelation' => 'hotel'],
            ['image_id', 'exist', 'targetRelation' => 'image'],
            ['name', 'string', 'max' => 255],
            ['price', 'double'],
            [['height', 'width', 'depth'], 'integer'],
            ['max_pets', 'integer', 'min' => 1],
            ['description', 'safe'],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => array_keys(static::getStatusList())],
        ];
    }

    public function attributeHints(): array
    {
        return array_merge(
            parent::attributeHints(),
            [
                'height' => Yii::t('app', 'Centimeters'),
                'width' => Yii::t('app', 'Centimeters'),
                'depth' => Yii::t('app', 'Centimeters'),
            ]
        );
    }
}