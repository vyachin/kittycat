<?php

namespace backend\models;

use common\components\PhoneValidator;

class HotelPhone extends \common\models\HotelPhone
{
    public function rules(): array
    {
        return [
            [['hotel_id', 'phone'], 'required'],
            ['hotel_id', 'exist', 'targetRelation' => 'hotel'],
            ['phone', PhoneValidator::class],
        ];
    }
}