<?php

namespace backend\models;

use Yii;

class Country extends \common\models\Country
{
    public static function getList(): array
    {
        return static::find()
            ->select(['name', 'id'])
            ->andWhere(['status' => self::STATUS_ACTIVE])
            ->orderBy(['name' => SORT_ASC])
            ->indexBy('id')
            ->column();
    }

    public function rules(): array
    {
        return [
            [['code', 'name'], 'trim'],
            [['code', 'name'], 'required'],
            ['code', 'match', 'pattern' => '#^[A-Z]{2}$#', 'message' => Yii::t('app', 'Latin alphas in upper case')],
            ['code', 'unique'],
            ['name', 'string', 'max' => 255],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => array_keys(static::getStatusList())],
        ];
    }
}