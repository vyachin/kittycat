<?php

namespace backend\models;

class Hotel extends \common\models\Hotel
{
    public function rules(): array
    {
        return [
            [['name', 'address', 'email', 'website', 'lat', 'lng'], 'trim'],
            [
                'website',
                'filter',
                'filter' => function ($url) {
                    return rtrim($url, '/');
                }
            ],
            [['name', 'address', 'website'], 'required'],
            [['name', 'address', 'email', 'website'], 'string', 'max' => 255],
            ['email', 'email'],
            ['website', 'url', 'defaultScheme' => 'http'],
            [['lat', 'lng'], 'double'],
            ['description', 'safe'],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => array_keys(static::getStatusList())],
        ];
    }

    public function getCountry()
    {
        return $this->hasOne(Country::class, ['id' => 'country_id']);
    }

    public function getCity()
    {
        return $this->hasOne(City::class, ['id' => 'city_id']);
    }

    public function getRooms()
    {
        return $this->hasMany(Room::class, ['hotel_id' => 'id']);
    }

    public function getHotelPhones()
    {
        return $this->hasMany(HotelPhone::class, ['hotel_id' => 'id']);
    }
}