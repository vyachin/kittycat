<?php

namespace backend\forms;

use backend\models\City;
use backend\models\Country;
use common\components\ValidationException;
use Yii;
use yii\base\Model;

class MultiCreateCityForm extends Model
{
    public $country_id;
    public $cities;

    public function rules()
    {
        return [
            ['country_id', 'required'],
            ['country_id', 'exist', 'targetClass' => Country::class, 'targetAttribute' => 'id'],
            ['cities', 'required'],
        ];
    }

    public function save()
    {
        if (!$this->validate()) {
            return false;
        }
        try {
            return Yii::$app->db->transaction(
                function () {
                    $cities = array_filter(array_map('trim', preg_split('#\n#', $this->cities)));
                    foreach ($cities as $cityName) {
                        $model = new City();
                        $model->country_id = $this->country_id;
                        $model->name = $cityName;
                        if (!$model->save()) {
                            $this->addErrors($model->errors);
                            throw new ValidationException();
                        }
                    }
                    return true;
                }
            );
        } catch (ValidationException $e) {
            return false;
        }
    }

    public function attributeLabels(): array
    {
        return [
            'country_id' => Yii::t('app', 'Country'),
            'cities' => Yii::t('app', 'Cities'),
        ];
    }
}