<?php

namespace backend\forms;

use backend\models\City;
use backend\models\Country;
use backend\models\Hotel;
use common\components\PhoneValidator;
use common\components\ValidationException;
use common\models\HotelImage;
use common\models\HotelPhone;
use common\models\Upload;
use Yii;
use yii\base\Exception;
use yii\helpers\VarDumper;

class HotelForm extends Hotel
{
    public $image_list;
    public $phone_list;

    public function afterFind()
    {
        parent::afterFind();

        $this->image_list = array_map(
            function (HotelImage $image) {
                return $image->image_id;
            },
            $this->hotelImages
        );
        $this->phone_list = array_map(
            function (HotelPhone $hotelPhone) {
                return $hotelPhone->phone;
            },
            $this->hotelPhones
        );
    }

    public function rules(): array
    {
        return array_merge(
            parent::rules(),
            [
                [
                    'image_list',
                    'exist',
                    'targetClass' => Upload::class,
                    'targetAttribute' => 'id',
                    'allowArray' => true
                ],
                ['phone_list', 'each', 'rule' => [PhoneValidator::class]],
            ]
        );
    }

    public function beforeValidate()
    {
        if ($this->isAttributeChanged('address')) {
            $this->country_id = $this->city_id = null;
            try {
                $coordinate = Yii::$app->geocoder->getCoordinate($this->address);
                $this->lat = $coordinate->lat;
                $this->lng = $coordinate->lng;
                $this->setCountryCode($coordinate->countryCode);
                $this->setCityName($coordinate->cityName);
            } catch (Exception $e) {
            }
        }
        return parent::beforeValidate();
    }


    private function setCountryCode($countryCode)
    {
        $country = Country::findOne(['code' => $countryCode]);
        if ($country) {
            $this->country_id = $country->id;
        } else {
            Yii::warning("Country '{$countryCode}' not found");
        }
    }

    private function setCityName($cityName)
    {
        $city = City::findOne(['name' => $cityName]);
        if ($city) {
            $this->city_id = $city->id;
        } else {
            Yii::warning("City '{$cityName}' not found");
        }
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        Yii::error($_POST);
        if (!$this->validate()) {
            return false;
        }

        try {
            return Yii::$app->db->transaction(
                function () {
                    if (!parent::save()) {
                        throw new ValidationException();
                    }

                    $hotelImages = $this->hotelImages;
                    if ($this->image_list) {
                        foreach ($this->image_list as $imageId) {
                            $model = array_shift($hotelImages);
                            if (!$model) {
                                $model = new HotelImage();
                                $model->hotel_id = $this->id;
                            }
                            $model->image_id = $imageId;
                            if (!$model->save()) {
                                $this->addErrors($model->errors);
                                throw new ValidationException();
                            }
                        }
                    }
                    if ($hotelImages) {
                        HotelImage::deleteAll(
                            [
                                'id' => array_map(
                                    function (HotelImage $hotelImage) {
                                        return $hotelImage->id;
                                    },
                                    $hotelImages
                                )
                            ]
                        );
                    }

                    $hotelPhones = $this->hotelPhones;
                    if ($this->phone_list) {
                        foreach ($this->phone_list as $phone) {
                            $model = array_shift($hotelPhones);
                            if (!$model) {
                                $model = new HotelPhone();
                                $model->hotel_id = $this->id;
                            }
                            $model->phone = $phone;
                            if (!$model->save()) {
                                $this->addErrors($model->errors);
                                throw new ValidationException();
                            }
                        }
                    }
                    if ($hotelPhones) {
                        HotelPhone::deleteAll(
                            [
                                'id' => array_map(
                                    function (HotelPhone $hotelPhone) {
                                        return $hotelPhone->id;
                                    },
                                    $hotelPhones
                                )
                            ]
                        );
                    }
                    return true;
                }
            );
        } catch (ValidationException $e) {
            return false;
        }
    }

    public function attributes()
    {
        return array_merge(parent::attributes(), ['image_list', 'phone_list']);
    }

    public function cloneHotel()
    {
        return Yii::$app->db->transaction(
            function () {
                $model = new self();
                $model->setAttributes($this->getAttributes());
                if (!$model->save()) {
                    Yii::error(VarDumper::dumpAsString([$model->attributes, $model->errors]), __METHOD__);
                    throw new Exception();
                }

                foreach (RoomForm::findAll(['hotel_id' => $this->id]) as $room) {
                    $roomForm = new RoomForm();
                    $roomForm->setAttributes($room->getAttributes());
                    $roomForm->hotel_id = $model->id;
                    if (!$roomForm->save()) {
                        Yii::error(VarDumper::dumpAsString([$roomForm->attributes, $roomForm->errors]), __METHOD__);
                        throw new Exception();
                    }
                }

                return $model;
            }
        );
    }
}