<?php

namespace backend\forms;

use backend\models\Room;
use common\components\ValidationException;
use common\models\RoomService;
use Yii;
use yii\helpers\ArrayHelper;

class RoomForm extends Room
{
    public $services;

    public function rules(): array
    {
        return array_merge(
            parent::rules(),
            [
                ['services', 'safe'],
            ]
        );
    }

    public function afterFind()
    {
        parent::afterFind();
        $this->services = ArrayHelper::map(
            $this->roomServices,
            'service_id',
            function (RoomService $value) {
                return [
                    'enabled' => true,
                    'price' => $value->price,
                    'service_id' => $value->service_id,
                ];
            }
        );
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        if (!$this->validate()) {
            return false;
        }
        try {
            return Yii::$app->db->transaction(
                function () {
                    if (!parent::save(false)) {
                        throw new ValidationException();
                    }

                    RoomService::deleteAll(['room_id' => $this->id]);
                    foreach ((array)$this->services as $service) {
                        if ($service['enabled'] ?? false) {
                            $model = new RoomService();
                            $model->room_id = $this->id;
                            $model->price = (float)($service['price'] ?? 0);
                            $model->service_id = $service['service_id'] ?? 0;
                            if (!$model->save()) {
                                $this->addErrors($model->errors);
                                throw new ValidationException();
                            }
                        }
                    }

                    return true;
                }
            );
        } catch (ValidationException $e) {
            return false;
        }
    }

    public function attributes()
    {
        return array_merge(parent::attributes(), ['services']);
    }
}