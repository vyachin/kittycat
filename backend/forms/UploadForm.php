<?php

namespace backend\forms;

use common\helpers\Image;
use common\models\Upload;
use Yii;
use yii\base\Model;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;

class UploadForm extends Model
{
    public const MAX_FILE_SIZE = 16 * 1024 * 1024;
    public const LEVELS = 3;

    /** @var UploadedFile */
    public $file;

    public $storage;

    public $width;
    public $height;

    /** @var Upload */
    private $_upload;

    public function init()
    {
        parent::init();
        $this->_upload = new Upload();
    }

    public function rules()
    {
        return [
            ['file', 'required'],
            ['file', 'file', 'maxSize' => self::MAX_FILE_SIZE],
            ['storage', 'default', 'value' => Upload::STORAGE_LOCAL],
            ['storage', 'in', 'range' => array_keys(Upload::getStorageList())],
            [['width', 'height'], 'integer'],
        ];
    }

    public function save(): bool
    {
        if (!$this->validate()) {
            return false;
        }

        $this->_upload->storage = $this->storage;
        $this->_upload->type = mime_content_type($this->file->tempName);
        $this->_upload->size = $this->file->size;
        $this->_upload->path = $this->generatePath($this->_upload->type, $this->file->tempName, $this->file->name);
        $storage = Yii::$app->fileStorage->get($this->storage);
        $storage->upload($this->file->tempName, $this->_upload->path, $this->file->type);
        @unlink($this->file->tempName);

        if (!$this->_upload->save()) {
            $this->addErrors($this->_upload->errors);
            return false;
        }
        return true;
    }

    private function generatePath($mimeType, $tempName, $fileName): string
    {
        $extensions = FileHelper::getExtensionsByMimeType($mimeType);
        if (count($extensions) == 1) {
            $extension = reset($extensions);
        } else {
            $extension = pathinfo($fileName, PATHINFO_EXTENSION);
        }
        $extension = mb_strtolower($extension, Yii::$app->charset);
        $hash = sha1_file($tempName);
        $path = '';
        for ($i = 0; $i < self::LEVELS; $i++) {
            $path .= substr($hash, $i * 3, 3) . '/';
        }
        $fileName = substr($hash, self::LEVELS * 3);
        return "{$path}{$fileName}.{$extension}";
    }

    public function fields()
    {
        return [
            'id' => function () {
                return $this->_upload->id;
            },
            'url' => function () {
                $url = $this->_upload->getUrl();
                if ($this->width || $this->height) {
                    $url = Image::resize($this->_upload->storage, $this->_upload->path, $this->width, $this->height);
                }
                return $url;
            },
        ];
    }
}
