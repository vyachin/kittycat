<?php

namespace backend\searches;

use common\components\SearchModelInterface;
use common\models\Model;
use yii\data\ActiveDataProvider;

class ModelSearch extends Model implements SearchModelInterface
{
    public function formName(): string
    {
        return '';
    }

    public function search($params = []): ActiveDataProvider
    {
        $query = static::find();

        return new ActiveDataProvider(['query' => $query]);
    }
}