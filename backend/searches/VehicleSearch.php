<?php

namespace backend\searches;

use common\components\SearchModelInterface;
use common\models\Vehicle;
use yii\data\ActiveDataProvider;

class VehicleSearch extends Vehicle implements SearchModelInterface
{
    public function formName(): string
    {
        return '';
    }

    public function search($params = []): ActiveDataProvider
    {
        $query = static::find()->with(['mark', 'model', 'salon']);

        return new ActiveDataProvider(['query' => $query]);
    }
}