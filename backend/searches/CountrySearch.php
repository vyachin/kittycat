<?php

namespace backend\searches;

use backend\models\Country;
use common\components\SearchModelInterface;
use yii\data\ActiveDataProvider;

class CountrySearch extends Country implements SearchModelInterface
{
    public function rules(): array
    {
        return [
            [['code', 'id', 'name', 'status'], 'safe'],
        ];
    }

    public function formName(): string
    {
        return '';
    }

    public function search($params = []): ActiveDataProvider
    {
        $this->load($params);
        $query = static::find();
        $query->andFilterWhere(
            [
                'AND',
                ['id' => $this->id, 'code' => $this->code, 'status' => $this->status],
                ['LIKE', 'name', $this->name],
            ]
        );
        return new ActiveDataProvider(['query' => $query]);
    }
}