<?php

namespace backend\searches;

use backend\models\Service;
use common\components\SearchModelInterface;
use yii\data\ActiveDataProvider;

class ServiceSearch extends Service implements SearchModelInterface
{
    public function formName(): string
    {
        return '';
    }

    public function search($params = []): ActiveDataProvider
    {
        $query = static::find();

        return new ActiveDataProvider(['query' => $query]);
    }
}