<?php

namespace backend\searches;

use common\components\SearchModelInterface;
use common\models\Salon;
use yii\data\ActiveDataProvider;

class SalonSearch extends Salon implements SearchModelInterface
{
    public function formName(): string
    {
        return '';
    }

    public function search($params = []): ActiveDataProvider
    {
        $query = static::find();

        return new ActiveDataProvider(['query' => $query]);
    }
}