<?php

namespace backend\searches;

use common\components\SearchModelInterface;
use common\models\Mark;
use yii\data\ActiveDataProvider;

class MarkSearch extends Mark implements SearchModelInterface
{
    public function formName(): string
    {
        return '';
    }

    public function search($params = []): ActiveDataProvider
    {
        $query = static::find();

        return new ActiveDataProvider(['query' => $query]);
    }
}