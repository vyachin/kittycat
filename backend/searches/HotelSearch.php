<?php

namespace backend\searches;

use backend\models\Hotel;
use common\components\SearchModelInterface;
use Yii;
use yii\data\ActiveDataProvider;

class HotelSearch extends Hotel implements SearchModelInterface
{
    public function formName(): string
    {
        return '';
    }

    public function search($params = []): ActiveDataProvider
    {
        $query = static::find();

        return new ActiveDataProvider(['query' => $query, 'sort' => ['defaultOrder' => ['id' => SORT_DESC]]]);
    }

    public function attributeLabels(): array
    {
        return array_merge(
            parent::attributeLabels(),
            [
                'country.name' => Yii::t('app', 'Country'),
                'city.name' => Yii::t('app', 'City'),
            ]
        );
    }
}