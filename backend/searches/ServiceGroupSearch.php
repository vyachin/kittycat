<?php

namespace backend\searches;

use backend\models\ServiceGroup;
use common\components\SearchModelInterface;
use yii\data\ActiveDataProvider;

class ServiceGroupSearch extends ServiceGroup implements SearchModelInterface
{
    public function formName(): string
    {
        return '';
    }

    public function search($params = []): ActiveDataProvider
    {
        $query = static::find();

        return new ActiveDataProvider(['query' => $query]);
    }
}