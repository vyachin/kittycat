<?php

namespace backend\searches;

use backend\models\City;
use common\components\SearchModelInterface;
use Yii;
use yii\data\ActiveDataProvider;

class CitySearch extends City implements SearchModelInterface
{
    public function formName(): string
    {
        return '';
    }

    public function search($params = []): ActiveDataProvider
    {
        $query = static::find();

        return new ActiveDataProvider(['query' => $query]);
    }

    public function attributeLabels(): array
    {
        return array_merge(
            parent::attributeLabels(),
            [
                'country.name' => Yii::t('app', 'Country'),
            ]
        );
    }
}