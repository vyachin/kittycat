<?php

namespace backend\controllers;

use backend\models\Country;
use backend\searches\CountrySearch;
use common\components\CreateAction;
use common\components\IndexAction;
use common\components\UpdateAction;
use yii\filters\AccessControl;
use yii\web\Controller;

class CountryController extends Controller
{
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actions(): array
    {
        return [
            'index' => [
                'class' => IndexAction::class,
                'modelClass' => CountrySearch::class,
            ],
            'create' => [
                'class' => CreateAction::class,
                'modelClass' => Country::class,
            ],
            'update' => [
                'class' => UpdateAction::class,
                'modelClass' => Country::class,
            ],
        ];
    }
}