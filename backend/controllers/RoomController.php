<?php

namespace backend\controllers;

use backend\forms\RoomForm;
use common\components\CreateAction;
use common\components\UpdateAction;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class RoomController extends Controller
{
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actions(): array
    {
        return [
            'create' => [
                'class' => CreateAction::class,
                'modelClass' => RoomForm::class,
                'dependencies' => ['hotel_id'],
            ],
            'update' => [
                'class' => UpdateAction::class,
                'modelClass' => RoomForm::class,
            ],
        ];
    }

    public function actionCopy($id)
    {
        $room = RoomForm::findOne($id);
        if (!$room) {
            throw new NotFoundHttpException();
        }
        $model = new RoomForm();
        $model->setAttributes($room->getAttributes());
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->asJson(['model' => $model]);
        }
        $form = $this->renderPartial('_form', ['model' => $model]);
        return $this->asJson(['form' => $form]);
    }
}