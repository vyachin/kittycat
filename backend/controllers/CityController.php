<?php

namespace backend\controllers;

use backend\forms\MultiCreateCityForm;
use backend\models\City;
use backend\searches\CitySearch;
use common\components\CreateAction;
use common\components\IndexAction;
use common\components\UpdateAction;
use yii\filters\AccessControl;
use yii\helpers\Html;
use yii\web\Controller;

class CityController extends Controller
{
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actions(): array
    {
        return [
            'index' => [
                'class' => IndexAction::class,
                'modelClass' => CitySearch::class,
            ],
            'create' => [
                'class' => CreateAction::class,
                'modelClass' => City::class,
            ],
            'multi-create' => [
                'class' => CreateAction::class,
                'modelClass' => MultiCreateCityForm::class,
                'view' => '_multi_create_form',
            ],
            'update' => [
                'class' => UpdateAction::class,
                'modelClass' => City::class,
            ],
        ];
    }

    public function actionList($country_id)
    {
        $options = [];
        $cities = City::getList($country_id);
        return Html::renderSelectOptions(key($cities), $cities, $options);
    }
}