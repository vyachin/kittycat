<?php

namespace backend\controllers;

use backend\models\ServiceGroup;
use backend\searches\ServiceGroupSearch;
use common\components\CreateAction;
use common\components\IndexAction;
use common\components\UpdateAction;
use yii\filters\AccessControl;
use yii\web\Controller;

class ServiceGroupController extends Controller
{
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actions(): array
    {
        return [
            'index' => [
                'class' => IndexAction::class,
                'modelClass' => ServiceGroupSearch::class,
            ],
            'create' => [
                'class' => CreateAction::class,
                'modelClass' => ServiceGroup::class,
            ],
            'update' => [
                'class' => UpdateAction::class,
                'modelClass' => ServiceGroup::class,
            ],
        ];
    }
}