<?php

namespace backend\controllers;

use backend\forms\HotelForm;
use backend\searches\HotelSearch;
use common\components\IndexAction;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class HotelController extends Controller
{
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actions(): array
    {
        return [
            'index' => [
                'class' => IndexAction::class,
                'modelClass' => HotelSearch::class,
            ],
        ];
    }

    public function actionCreate()
    {
        $model = new HotelForm();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['detail', 'id' => $model->id]);
        }
        return $this->render('create', ['model' => $model]);
    }

    public function actionDetail($id)
    {
        $model = HotelForm::findOne($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->refresh();
        }
        return $this->render('detail', ['model' => $model]);
    }

    public function actionImage($id)
    {
        $model = HotelForm::findOne($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->refresh();
        }
        return $this->render('image', ['model' => $model]);
    }

    public function actionRoom($id)
    {
        $model = HotelForm::findOne($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->refresh();
        }
        $dataProvider = new ActiveDataProvider(['query' => $model->getRooms()]);
        return $this->render('room', ['model' => $model, 'dataProvider' => $dataProvider]);
    }

    public function actionCopy($id)
    {
        $hotel = HotelForm::findOne($id);
        if (!$hotel) {
            throw new NotFoundHttpException();
        }
        $model = $hotel->cloneHotel();
        return $this->render('create', ['model' => $model]);
    }
}