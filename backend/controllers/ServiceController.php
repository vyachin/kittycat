<?php

namespace backend\controllers;

use backend\models\Service;
use backend\searches\ServiceSearch;
use common\components\CreateAction;
use common\components\IndexAction;
use common\components\UpdateAction;
use yii\filters\AccessControl;
use yii\web\Controller;

class ServiceController extends Controller
{
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actions(): array
    {
        return [
            'index' => [
                'class' => IndexAction::class,
                'modelClass' => ServiceSearch::class,
            ],
            'create' => [
                'class' => CreateAction::class,
                'modelClass' => Service::class,
            ],
            'update' => [
                'class' => UpdateAction::class,
                'modelClass' => Service::class,
            ],
        ];
    }
}