<?php

namespace backend\controllers;

use backend\forms\LoginForm;
use backend\forms\UploadForm;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\ErrorAction;
use yii\web\UploadedFile;

class SiteController extends Controller
{
    public function actions(): array
    {
        return [
            'error' => ErrorAction::class,
        ];
    }

    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['logout', 'index'],
                'rules' => [
                    [
                        'actions' => ['logout', 'index', 'upload'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->goHome();
    }

    public function actionLogin()
    {
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goHome();
        }
        return $this->render('login', ['model' => $model]);
    }

    public function actionUpload()
    {
        if (!Yii::$app->request->isPost) {
            throw new BadRequestHttpException();
        }

        $model = new UploadForm();
        $model->file = UploadedFile::getInstanceByName('file');
        $model->load(Yii::$app->request->post(), '');
        if ($model->save()) {
            return $this->asJson(['success' => true, 'model' => $model]);
        }
        return $this->asJson(['success' => false, 'errors' => $model->errors]);
    }
}