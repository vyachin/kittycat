<?php

use common\models\User;
use yii\redis\Session;
use yii\web\Cookie;

return [
    'id' => 'kittycat-backend',
    'name' => 'Admin',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'components' => [
        'assetManager' => [
            'bundles' => false,
        ],
        'errorHandler' => [
            'errorAction' => YII_DEBUG ? null : 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
        ],
        'session' => [
            'class' => Session::class,
            'redis' => 'redis',
        ],
        'user' => [
            'identityClass' => User::class,
            'enableAutoLogin' => true,
            'identityCookie' => [
                'name' => 'kittycat-identity-backend',
                'httpOnly' => true,
                'sameSite' => Cookie::SAME_SITE_LAX
            ],
        ],
        'log' => [
            'traceLevel' => 3,
            'targets' => [
                'error' => [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                    'logFile' => '@runtime/logs/backend.error.log',
                ],
            ],
        ],
    ],
];