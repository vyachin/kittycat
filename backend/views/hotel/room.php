<?php
/**
 * @var $this View
 * @var $content string
 * @var $model Hotel
 * @var $dataProvider \yii\data\ActiveDataProvider
 */

use backend\models\Hotel;
use backend\models\Room;
use common\helpers\Image;
use yii\bootstrap4\Html;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\web\View;

$this->registerJs("KK.HotelRoom();");
$this->title = Yii::t('app', 'Update hotel {hotel} rooms', ['hotel' => $model->name]);
$this->beginContent('@backend/views/hotel/layout.php', ['model' => $model]);
?>
<?= Html::a(
    Yii::t('app', 'Add'),
    ['/room/create', 'hotel_id' => $model->id],
    ['class' => 'js-show-room-modal btn btn-primary btn-sm']
) ?>
<?= GridView::widget(
    [
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'attribute' => 'image_id',
                'format' => 'image',
                'value' => function (Room $room) {
                    $image = $room->image;
                    if ($image) {
                        return Image::resize($image->storage, $image->path, null, 50);
                    }
                    return null;
                }
            ],
            'name',
            [
                'header' => Yii::t('app', 'Sizes'),
                'value' => function (Room $room) {
                    if ($room->height && $room->width && $room->depth) {
                        return implode(' x ', [$room->height, $room->width, $room->depth]);
                    }
                    return null;
                }
            ],
            'price',
            'max_pets',
            'status:roomStatus',
            [
                'class' => ActionColumn::class,
                'template' => '{update} {copy}',
                'buttons' => [
                    'update' => function ($url, $model) {
                        unset($url);
                        return Html::a(
                            '<i class="fas fa-pen"></i>',
                            ['/room/update', 'id' => $model->id],
                            ['class' => 'js-show-room-modal', 'title' => Yii::t('app', 'Update')]
                        );
                    },
                    'copy' => function ($url, $model) {
                        unset($url);
                        return Html::a(
                            '<i class="fas fa-copy"></i>',
                            ['/room/copy', 'id' => $model->id],
                            ['class' => 'js-show-room-modal', 'title' => Yii::t('app', 'Copy')]
                        );
                    },
                ],
            ],
        ]
    ]
); ?>
<?php $this->endContent(); ?>
