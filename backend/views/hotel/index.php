<?php
/**
 * @var View $this
 * @var HotelSearch $searchModel
 * @var ActiveDataProvider $dataProvider
 */

use backend\models\Hotel;
use backend\models\HotelPhone;
use backend\searches\HotelSearch;
use yii\bootstrap4\Html;
use yii\data\ActiveDataProvider;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\web\View;

$this->title = 'Hotels';
$this->registerJs('KK.HotelList();');
$this->params['breadcrumbs'] = [Yii::t('app', 'Hotels')];
?>
<h1><?= $this->title ?></h1>
<?= Html::a('Create', ['create'], ['class' => 'btn btn-primary btn-sm']) ?>
<?= GridView::widget(
    [
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            'slug',
            [
                'header' => Yii::t('app', 'Address'),
                'format' => 'raw',
                'value' => function (Hotel $model) {
                    return ArrayHelper::getValue($model, 'country.name') . ' / ' .
                        ArrayHelper::getValue($model, 'city.name') . " ({$model->lat} , {$model->lng})<br>" .
                        Html::encode($model->address);
                }
            ],
            'name',
            [
                'header' => Yii::t('app', 'Contacts'),
                'format' => 'raw',
                'value' => function (Hotel $model) {
                    $phones = implode(
                        ', ',
                        array_map(
                            function (HotelPhone $hotelPhone) {
                                return $hotelPhone->phone;
                            },
                            $model->hotelPhones
                        )
                    );
                    return implode(
                        '<br>',
                        array_filter(
                            [
                                Html::encode($phones),
                                Html::encode($model->email),
                                Html::encode($model->website),
                            ]
                        )
                    );
                }
            ],
            'status:hotelStatus',
            [
                'class' => ActionColumn::class,
                'template' => '{detail} {copy}',
                'buttons' => [
                    'detail' => function ($url) {
                        return Html::a(
                            '<i class="fas fa-pen"></i>',
                            $url,
                            ['title' => Yii::t('app', 'Details')]
                        );
                    },
                    'copy' => function ($url) {
                        return Html::a(
                            '<i class="fas fa-copy"></i>',
                            $url,
                            ['title' => Yii::t('app', 'Copy')]
                        );
                    },
                ],
            ],
        ]
    ]
); ?>
