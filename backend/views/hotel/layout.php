<?php
/**
 * @var $this View
 * @var $content string
 * @var $model Hotel
 */

use backend\models\Hotel;
use yii\bootstrap4\Html;
use yii\bootstrap4\Tabs;
use yii\helpers\Url;
use yii\web\View;

$currentUrl = Url::current();
$this->params['breadcrumbs'] = [
    [
        'label' => Yii::t('app', 'Hotels'),
        'url' => ['hotel/index'],
    ],
    $model->name,
];
?>
<div class="hotel-update">
    <div class="h2">
        <?= Html::encode($this->title) ?>
    </div>
    <?= Tabs::widget(
        [
            'items' => [
                [
                    'label' => Yii::t('app', 'Details'),
                    'url' => ['detail', 'id' => $model->id],
                    'active' => $currentUrl == Url::to(['detail', 'id' => $model->id])
                ],
                [
                    'label' => Yii::t('app', 'Images'),
                    'url' => ['image', 'id' => $model->id],
                    'active' => $currentUrl == Url::to(['image', 'id' => $model->id])
                ],
                [
                    'label' => Yii::t('app', 'Rooms'),
                    'url' => ['room', 'id' => $model->id],
                    'active' => $currentUrl == Url::to(['room', 'id' => $model->id])
                ],
            ]
        ]
    ); ?>
    <?= $content ?>
</div>

