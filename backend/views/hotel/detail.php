<?php
/**
 * @var $this View
 * @var $content string
 * @var $model Hotel
 */

use backend\models\Hotel;
use yii\helpers\Json;
use yii\web\View;

$this->registerJsFile(
    'https://maps.googleapis.com/maps/api/js?' . http_build_query(
        [
            'v' => 3,
            'region' => substr(Yii::$app->language, 0, 2),
            'language' => Yii::$app->language,
            'key' => Yii::$app->params['googleMapsApiKey'],
            'callback' => 'KK.initHotelMap',
        ]
    )
);
$phones = [];
foreach ($model->hotelPhones as $hotelPhone) {
    $phones[] = $hotelPhone->phone;
}
$options = Json::encode(['formName' => $model->formName(), 'phones' => $phones]);
$this->registerJs("KK.HotelDetail({$options});");
$this->title = Yii::t('app', 'Update hotel {hotel}', ['hotel' => $model->name]);
$this->beginContent('@backend/views/hotel/layout.php', ['model' => $model]);
echo $this->render('_form', ['model' => $model]);
$this->endContent();
