<?php
/**
 * @var $this View
 * @var $content string
 * @var $model Hotel
 */

use backend\models\Hotel;
use common\helpers\Image;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\helpers\Json;
use yii\web\View;

$images = [];

foreach ($model->images as $image) {
    $images[] = [
        'url' => Image::crop($image->storage, $image->path, 240, 160),
        'id' => $image->id,
    ];
}
$options = Json::encode(['images' => $images, 'formName' => $model->formName()]);
$this->registerJs("KK.HotelImage({$options});");
$this->title = Yii::t('app', 'Update hotel {hotel} images', ['hotel' => $model->name]);
$this->beginContent('@backend/views/hotel/layout.php', ['model' => $model]);
?>
<?php $form = ActiveForm::begin(
    [
        'method' => 'post',
        'options' => ['autocomplete' => 'off']
    ]
); ?>
<div class="hotel-update-image">
    <?= $form->field($model, 'image_list', ['template' => '{input}'])->hiddenInput(['value' => '']); ?>
    <div id="js-image-list" class="image-list"></div>
</div>
<div class="hotel-update-bottom mt-4">
    <div class="row">
        <div class="col-12 d-flex justify-content-center">
            <?= Html::submitButton(
                Yii::t('app', 'Update'),
                ['class' => 'btn btn-sm btn-primary btn-page']
            ) ?>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>
<?php $this->endContent(); ?>
