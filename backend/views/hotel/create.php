<?php

/**
 * @var $this View
 * @var $model Hotel
 * @var $roomDataProvider ActiveDataProvider
 */

use backend\models\Hotel;
use yii\bootstrap4\Html;
use yii\data\ActiveDataProvider;
use yii\helpers\Json;
use yii\web\View;

$this->registerJsFile(
    'https://maps.googleapis.com/maps/api/js?' . http_build_query(
        [
            'v' => 3,
            'region' => substr(Yii::$app->language, 0, 2),
            'language' => Yii::$app->language,
            'key' => Yii::$app->params['googleMapsApiKey'],
            'callback' => 'KK.initHotelMap',
        ]
    )
);
$this->title = Yii::t('app', 'Create hotel');
$phones = [];
foreach ($model->hotelPhones as $hotelPhone) {
    $phones[] = $hotelPhone->phone;
}
$options = Json::encode(['formName' => $model->formName(), 'phones' => $phones]);
$this->registerJs("KK.HotelCreate({$options});");
?>

<div class="hotel-update">
    <div class="h2">
        <?= Html::encode($this->title) ?>
    </div>
    <?= $this->render('_form', ['model' => $model]) ?>
</div>