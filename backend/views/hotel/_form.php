<?php
/**
 * @var View $this
 * @var Hotel $model
 */

use backend\models\Hotel;
use common\widgets\Select;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\web\View;

?>
<?= Html::errorSummary($model) ?>
<?php $form = ActiveForm::begin(
    [
        'method' => 'post',
        'options' => ['autocomplete' => 'off']
    ]
); ?>
<div class="hotel-update-form">
    <div class="row">
        <div class="col-12">
            <?= $form->field($model, 'name')->textInput(['class' => 'form-control input-sm']); ?>
        </div>
        <div class="col-12">
            <?= $form->field($model, 'address')->textInput(['class' => 'form-control input-sm']); ?>
        </div>
        <div class="col-12">
            <div id="js-maps"></div>
        </div>
        <?= $form->field($model, 'lat', ['template' => '{input}'])->hiddenInput(['id' => 'js-lat',]) ?>
        <?= $form->field($model, 'lng', ['template' => '{input}'])->hiddenInput(['id' => 'js-lng',]) ?>
        <div class="col-12">
            <?= $form->field($model, 'description')->textarea(['class' => 'form-control input-sm', 'rows' => 5]); ?>
        </div>
        <?= $form->field($model, 'phone_list', ['template' => '{input}'])->hiddenInput(['value' => '']); ?>
        <div class="col-12">
            <label>Телефоны</label>
            <div id="js-phone-list" class="phone-list"></div>
        </div>
        <div class="col-6">
            <?= $form->field($model, 'email')->textInput(['class' => 'form-control input-sm']); ?>
        </div>
        <div class="col-6">
            <?= $form->field($model, 'website')->textInput(['class' => 'form-control input-sm']); ?>
        </div>
        <div class="col-4">
            <?= $form->field($model, 'status')->widget(Select::class, ['items' => Hotel::getStatusList()]); ?>
        </div>
    </div>
</div>
<div class="hotel-update-bottom mt-4">
    <div class="row">
        <div class="col-12 d-flex justify-content-center">
            <?= Html::submitButton(
                $model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'),
                ['class' => 'btn btn-sm btn-primary btn-page']
            ) ?>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>

