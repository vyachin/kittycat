<?php
/**
 * @var View $this
 * @var string $content
 */

use common\helpers\Asset;
use yii\bootstrap4\Breadcrumbs;
use yii\bootstrap4\Html;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;
use yii\web\View;

$logoutItem = Html::tag(
    'li',
    Html::beginForm(['/site/logout']) . Html::submitInput(
        Yii::t('app', 'Quit'),
        ['class' => 'btn btn-link text-decoration-none border-0 nav-link']
    ) . Html::endForm(),
    ['class' => 'nav-item']
);
$isAuthorizedUser = !Yii::$app->user->isGuest;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?= Html::encode($this->title) ?></title>
    <?= Html::cssFile(Asset::staticUrl('css/backend.css')) ?>
    <?= Html::csrfMetaTags() ?>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<?php NavBar::begin(['brandLabel' => Yii::$app->name, 'collapseOptions' => ['class' => 'justify-content-end']]); ?>
<?= Nav::widget(
    [
        'items' => [
            ['label' => Yii::t('app', 'Home'), 'url' => ['/site/index'], 'visible' => $isAuthorizedUser],
            ['label' => Yii::t('app', 'Countries'), 'url' => ['/country/index'], 'visible' => $isAuthorizedUser],
            ['label' => Yii::t('app', 'Cities'), 'url' => ['/city/index'], 'visible' => $isAuthorizedUser],
            [
                'label' => Yii::t('app', 'Service Groups'),
                'url' => ['/service-group/index'],
                'visible' => $isAuthorizedUser
            ],
            ['label' => Yii::t('app', 'Services'), 'url' => ['/service/index'], 'visible' => $isAuthorizedUser],
            ['label' => Yii::t('app', 'Hotels'), 'url' => ['/hotel/index'], 'visible' => $isAuthorizedUser],
            $isAuthorizedUser ? $logoutItem : '',
        ],
        'options' => ['class' => 'navbar-nav'],
    ]
);
?>
<?php NavBar::end() ?>
<div class="container">
    <?= Breadcrumbs::widget(['links' => $this->params['breadcrumbs'] ?? [],]); ?>
    <?= $content ?>
</div>
<?= Html::jsFile(Asset::staticUrl('js/backend.js')) ?>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

