<?php
/**
 * @var View $this
 * @var LoginForm $model
 */

use backend\forms\LoginForm;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\web\View;

$this->title = 'Вход';
?>
<div class="row">
    <div class="offset-3 col-6">
        <h1><?= $this->title ?></h1>
        <?php $form = ActiveForm::begin(['options' => ['autocomplete' => 'off']]) ?>
        <?= $form->field($model, 'email')->textInput() ?>
        <?= $form->field($model, 'password')->passwordInput() ?>
        <?= $form->field($model, 'rememberMe')->checkbox() ?>
        <?= Html::submitButton(Yii::t('app', 'Войти'), ['class' => 'btn btn-primary']) ?>
        <?php ActiveForm::end() ?>
    </div>
</div>