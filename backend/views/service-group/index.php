<?php
/**
 * @var View $this
 * @var ServiceGroupSearch $searchModel
 * @var ActiveDataProvider $dataProvider
 */

use backend\searches\ServiceGroupSearch;
use yii\bootstrap4\Html;
use yii\data\ActiveDataProvider;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\web\View;

$this->title = 'Services Group';
$this->registerJs('KK.ServiceGroupList();');
$this->params['breadcrumbs'] = [Yii::t('app', 'Service groups')];
?>
<h1><?= $this->title ?></h1>

<?= Html::a(Yii::t('app', 'Add'), ['create'], ['class' => 'js-show-modal btn btn-primary btn-sm']) ?>

<?= GridView::widget(
    [
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            'slug',
            'name',
            'status:serviceGroupStatus',
            [
                'class' => ActionColumn::class,
                'template' => '{update}',
                'buttons' => [
                    'update' => function ($url) {
                        return Html::a(
                            '<i class="fas fa-pen"></i>',
                            $url,
                            ['class' => 'js-show-modal', 'title' => Yii::t('app', 'Update')]
                        );
                    },
                ],
            ],
        ]
    ]
); ?>
