<?php
/**
 * @var View $this
 * @var MultiCreateCityForm $model
 */

use backend\forms\MultiCreateCityForm;
use backend\models\Country;
use common\widgets\Select;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\bootstrap4\Modal;
use yii\web\View;

$title = Yii::t('app', 'Add cities');

$submitButton = Html::button(
    Yii::t('app', 'Add'),
    ['class' => 'btn btn-sm btn-primary js-submit']
);

$closeButton = Html::button(Yii::t('app', 'Close'), ['class' => 'btn btn-sm btn-default', 'data-dismiss' => 'modal']);
?>
<?php Modal::begin(['title' => $title, 'footer' => $submitButton . ' ' . $closeButton,]); ?>
<?= Html::errorSummary($model) ?>
<?php $form = ActiveForm::begin(
    [
        'method' => 'post',
        'options' => ['autocomplete' => 'off']
    ]
); ?>
<?= $form->field($model, 'country_id')->widget(Select::class, ['items' => Country::getList()]); ?>
<?= $form->field($model, 'cities')->textarea(['class' => 'form-control input-sm']); ?>
<?php ActiveForm::end(); ?>
<?php Modal::end();
