<?php
/**
 * @var View $this
 * @var City $model
 */

use backend\models\City;
use backend\models\Country;
use common\widgets\Select;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\bootstrap4\Modal;
use yii\web\View;

$title = $model->isNewRecord ? Yii::t('app', 'Add city') : Yii::t('app', 'Update city');

$submitButton = Html::button(
    $model->isNewRecord ? Yii::t('app', 'Add') : Yii::t('app', 'Update'),
    ['class' => 'btn btn-sm btn-primary js-submit']
);

$closeButton = Html::button(Yii::t('app', 'Close'), ['class' => 'btn btn-sm btn-default', 'data-dismiss' => 'modal']);
?>
<?php Modal::begin(['title' => $title, 'footer' => $submitButton . ' ' . $closeButton,]); ?>
<?= Html::errorSummary($model) ?>
<?php $form = ActiveForm::begin(
    [
        'method' => 'post',
        'options' => ['autocomplete' => 'off']
    ]
); ?>
<?= $form->field($model, 'country_id')->widget(Select::class, ['items' => Country::getList()]); ?>
<?= $form->field($model, 'name')->textInput(['class' => 'form-control input-sm']); ?>
<?= $form->field($model, 'status')->widget(Select::class, ['items' => City::getStatusList()]); ?>
<?php ActiveForm::end(); ?>
<?php Modal::end();
