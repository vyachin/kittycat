<?php
/**
 * @var View $this
 * @var CitySearch $searchModel
 * @var ActiveDataProvider $dataProvider
 */

use backend\searches\CitySearch;
use yii\bootstrap4\Html;
use yii\data\ActiveDataProvider;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\web\View;

$this->title = 'Cities';
$this->registerJs('KK.CityList();');
$this->params['breadcrumbs'] = [Yii::t('app', 'Cities')];
?>
<h1><?= $this->title ?></h1>

<?= Html::a(Yii::t('app', 'Add'), ['create'], ['class' => 'js-show-modal btn btn-primary btn-sm']) ?>

<?= Html::a(Yii::t('app', 'Multi add cities'), ['multi-create'], ['class' => 'js-show-modal btn btn-primary btn-sm']) ?>

<?= GridView::widget(
    [
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            'slug',
            'country.name',
            'name',
            'status:cityStatus',
            [
                'class' => ActionColumn::class,
                'template' => '{update}',
                'buttons' => [
                    'update' => function ($url) {
                        return Html::a(
                            '<i class="fas fa-pen"></i>',
                            $url,
                            ['class' => 'js-show-modal', 'title' => Yii::t('app', 'Update')]
                        );
                    },
                ],
            ],
        ]
    ]
); ?>
