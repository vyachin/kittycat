<?php
/**
 * @var View $this
 * @var CountrySearch $searchModel
 * @var ActiveDataProvider $dataProvider
 */

use backend\searches\CountrySearch;
use yii\bootstrap4\Html;
use yii\data\ActiveDataProvider;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\web\View;

$this->title = 'Countries';
$this->registerJs('KK.CountryList();');
$this->params['breadcrumbs'] = [Yii::t('app', 'Countries')];

?>
<h1><?= $this->title ?></h1>
<?= Html::a('Add', ['create'], ['class' => 'js-show-modal btn btn-primary btn-sm']) ?>
<?= GridView::widget(
    [
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            'code',
            'name',
            'status:countryStatus',
            [
                'class' => ActionColumn::class,
                'template' => '{update}',
                'buttons' => [
                    'update' => function ($url) {
                        return Html::a(
                            '<i class="fas fa-pen"></i>',
                            $url,
                            ['class' => 'js-show-modal', 'title' => Yii::t('app', 'Update')]
                        );
                    },
                ],
            ],
        ]
    ]
); ?>
