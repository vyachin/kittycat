<?php
/**
 * @var View $this
 * @var ServiceSearch $searchModel
 * @var ActiveDataProvider $dataProvider
 */

use backend\searches\ServiceSearch;
use yii\bootstrap4\Html;
use yii\data\ActiveDataProvider;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\web\View;

$this->title = 'Services';
$this->registerJs('KK.ServiceList();');
$this->params['breadcrumbs'] = [Yii::t('app', 'Services')];
?>
<h1><?= $this->title ?></h1>

<?= Html::a(Yii::t('app', 'Add'), ['create'], ['class' => 'js-show-modal btn btn-primary btn-sm']) ?>

<?= GridView::widget(
    [
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            'slug',
            'serviceGroup.name',
            'name',
            'status:serviceStatus',
            [
                'class' => ActionColumn::class,
                'template' => '{update}',
                'buttons' => [
                    'update' => function ($url) {
                        return Html::a(
                            '<i class="fas fa-pen"></i>',
                            $url,
                            ['class' => 'js-show-modal', 'title' => Yii::t('app', 'Update')]
                        );
                    },
                ],
            ],
        ]
    ]
); ?>
