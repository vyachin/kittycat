<?php
/**
 * @var View $this
 * @var Service $model
 */

use backend\models\Service;
use backend\models\ServiceGroup;
use common\widgets\Select;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\bootstrap4\Modal;
use yii\web\View;

$title = $model->isNewRecord ? Yii::t('app', 'Add service') : Yii::t('app', 'Update service');

$submitButton = Html::button(
    $model->isNewRecord ? Yii::t('app', 'Add') : Yii::t('app', 'Update'),
    ['class' => 'btn btn-sm btn-primary js-submit']
);

$closeButton = Html::button(Yii::t('app', 'Close'), ['class' => 'btn btn-sm btn-default', 'data-dismiss' => 'modal']);
?>
<?php Modal::begin(['title' => $title, 'footer' => $submitButton . ' ' . $closeButton,]); ?>
<?= Html::errorSummary($model) ?>
<?php $form = ActiveForm::begin(
    [
        'method' => 'post',
        'options' => ['autocomplete' => 'off']
    ]
); ?>
<?= $form->field($model, 'service_group_id')->widget(Select::class, ['items' => ServiceGroup::getList()]); ?>
<?= $form->field($model, 'name')->textInput(['class' => 'form-control input-sm']); ?>
<?= $form->field($model, 'status')->widget(Select::class, ['items' => Service::getStatusList()]); ?>
<?php ActiveForm::end(); ?>
<?php Modal::end();
