<?php
/**
 * @var View $this
 * @var Room $model
 */

use backend\models\Room;
use backend\models\Service;
use common\helpers\Image;
use common\widgets\Select;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\bootstrap4\Modal;
use yii\web\View;

$title = $model->isNewRecord ? Yii::t('app', 'Add room') : Yii::t('app', 'Update room');

$submitButton = Html::button(
    $model->isNewRecord ? Yii::t('app', 'Add') : Yii::t('app', 'Update'),
    ['class' => 'btn btn-sm btn-primary js-submit']
);
$image = $model->image;
$closeButton = Html::button(Yii::t('app', 'Close'), ['class' => 'btn btn-sm btn-default', 'data-dismiss' => 'modal']);
?>
<?php Modal::begin(['title' => $title, 'footer' => $submitButton . ' ' . $closeButton, 'size' => Modal::SIZE_LARGE]); ?>
<?= Html::errorSummary($model) ?>
<?php $form = ActiveForm::begin(
    [
        'method' => 'post',
        'options' => ['autocomplete' => 'off']
    ]
); ?>
<?= $form->field($model, 'hotel_id')->label(false)->hiddenInput(); ?>
<?= $form->field($model, 'name')->textInput(['class' => 'form-control input-sm']); ?>
<?= $form->field($model, 'max_pets')->textInput(['class' => 'form-control input-sm']); ?>
    <div id="js-room-image">
        <?= $form->field($model, 'image_id')->hiddenInput(); ?>
        <?= Html::img(
            $image ? Image::resize($image->storage, $image->path, null, 100) : Image::NO_IMAGE,
            ['height' => 100]
        ) ?>
        <button class="btn btn-sm btn-primary js-upload" type="button"><?= Yii::t('app', 'Choose') ?></button>
        <button class="btn btn-sm btn-default js-clean" type="button"><?= Yii::t('app', 'Clean') ?></button>
        <p class="invalid-feedback"></p>
    </div>
    <div class="row">
        <div class="col-4">
            <?= $form->field($model, 'height')->textInput(['class' => 'form-control input-sm']); ?>
        </div>
        <div class="col-4">
            <?= $form->field($model, 'width')->textInput(['class' => 'form-control input-sm']); ?>
        </div>
        <div class="col-4">
            <?= $form->field($model, 'depth')->textInput(['class' => 'form-control input-sm']); ?>
        </div>
    </div>
<?= $form->field($model, 'description')->textarea(['class' => 'form-control input-sm', 'rows' => 5]); ?>
    <table class="table">
        <tr>
            <th><?= Yii::t('app', 'Service') ?></th>
            <th><?= Yii::t('app', 'Price') ?></th>
        </tr>
        <?php foreach (Service::getList() as $groupName => $services): ?>
            <tr>
                <td colspan="2"><?= Html::encode($groupName); ?></td>
            </tr>
            <?php foreach ($services as $serviceId => $serviceName): ?>
                <tr>
                    <td>
                        <?= Html::activeHiddenInput(
                            $model,
                            "services[$serviceId][service_id]",
                            ['value' => $serviceId]
                        ) ?>
                        <label>
                            <?= Html::activeCheckbox($model, "services[$serviceId][enabled]", ['label' => false]) ?>
                            <?= Html::encode($serviceName) ?>
                        </label>
                    </td>
                    <td>
                        <?= Html::activeTextInput($model, "services[$serviceId][price]", []) ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        <?php endforeach; ?>
    </table>
    <div class="row">
        <div class="col-6">
            <?= $form->field($model, 'price')->textInput(['class' => 'form-control input-sm']); ?>
        </div>
        <div class="col-6">
            <?= $form->field($model, 'status')->widget(Select::class, ['items' => Room::getStatusList()]); ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>
<?php Modal::end();
