<?php

namespace console\forms;

class User extends \common\models\User
{
    public $password;

    public function rules(): array
    {
        return [
            [['email', 'password'], 'required'],
            ['email', 'string', 'max' => 255],
            ['email', 'email'],
            ['email', 'unique'],
        ];
    }

    /**
     * @param bool $insert
     * @return bool
     * @throws \yii\base\Exception
     */
    public function beforeSave($insert)
    {
        $this->updatePassword($this->password);
        $this->generateAccessToken();
        $this->generateAuthKey();
        $this->role = self::ROLE_ADMIN;
        $this->status = self::STATUS_ACTIVE;
        return parent::beforeSave($insert);
    }

}