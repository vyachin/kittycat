<?php

return [
    'id' => 'kittycat-console',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'console\controllers',
    'components' => [
        'log' => [
            'traceLevel' => 3,
            'targets' => [
                'error' => [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                    'logFile' => '@runtime/logs/console.error.log',
                ],
            ],
        ],
    ],
];