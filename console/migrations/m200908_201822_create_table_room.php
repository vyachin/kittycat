<?php

use yii\db\Migration;

/**
 * Class m200908_201822_create_table_room
 */
class m200908_201822_create_table_room extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(
            'room',
            [
                'id' => $this->primaryKey(),
                'hotel_id' => $this->integer()->notNull(),
                'name' => $this->string()->notNull(),
                'description' => $this->text(),
                'price' => $this->decimal(10, 2)->notNull(),
                'status' => $this->tinyInteger()->notNull(),
                'created_at' => $this->dateTime()->notNull(),
                'created_ip' => $this->integer()->unsigned(),
                'created_by' => $this->integer(),
                'updated_at' => $this->dateTime()->notNull(),
                'updated_ip' => $this->integer()->unsigned(),
                'updated_by' => $this->integer(),
            ],
            'ENGINE=InnoDB DEFAULT CHARSET=utf8mb4'
        );

        $this->addForeignKey('fk_room_hotel_id', 'room', 'hotel_id', 'hotel', 'id');
        $this->addForeignKey('fk_room_created_by', 'room', 'created_by', 'user', 'id');
        $this->addForeignKey('fk_room_updated_by', 'room', 'updated_by', 'user', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('room');
    }
}
