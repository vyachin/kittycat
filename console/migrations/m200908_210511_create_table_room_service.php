<?php

use yii\db\Migration;

/**
 * Class m200908_210511_create_table_room_service_service
 */
class m200908_210511_create_table_room_service extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(
            'room_service',
            [
                'id' => $this->primaryKey(),
                'room_id' => $this->integer()->notNull(),
                'service_id' => $this->integer()->notNull(),
                'price' => $this->decimal(10, 2)->notNull(),
                'created_at' => $this->dateTime()->notNull(),
                'created_ip' => $this->integer()->unsigned(),
                'created_by' => $this->integer(),
            ],
            'ENGINE=InnoDB DEFAULT CHARSET=utf8mb4'
        );
        $this->createIndex('uq_room_id_service_id', 'room_service', ['room_id', 'service_id'], true);
        $this->addForeignKey('fk_room_service_room_id', 'room_service', 'room_id', 'room', 'id');
        $this->addForeignKey('fk_room_service_service_id', 'room_service', 'service_id', 'service', 'id');
        $this->addForeignKey('fk_room_service_created_by', 'room_service', 'created_by', 'user', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('room_service');
    }
}
