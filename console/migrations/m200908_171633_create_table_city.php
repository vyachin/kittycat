<?php

use yii\db\Migration;

/**
 * Class m200908_171633_create_table_city
 */
class m200908_171633_create_table_city extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(
            'city',
            [
                'id' => $this->primaryKey(),
                'country_id' => $this->integer()->notNull(),
                'slug' => $this->string()->notNull()->unique(),
                'name' => $this->string()->notNull(),
                'status' => $this->tinyInteger()->notNull(),
                'created_at' => $this->dateTime()->notNull(),
                'created_ip' => $this->integer()->unsigned(),
                'created_by' => $this->integer(),
                'updated_at' => $this->dateTime()->notNull(),
                'updated_ip' => $this->integer()->unsigned(),
                'updated_by' => $this->integer(),
            ],
            'ENGINE=InnoDB DEFAULT CHARSET=utf8mb4'
        );

        $this->addForeignKey('fk_city_country_id', 'city', 'country_id', 'country', 'id');
        $this->addForeignKey('fk_city_created_by', 'city', 'created_by', 'user', 'id');
        $this->addForeignKey('fk_city_updated_by', 'city', 'updated_by', 'user', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('country');
    }
}
