<?php

use yii\db\Migration;

/**
 * Class m201203_192927_create_table_hotel_phone
 */
class m201203_192927_create_table_hotel_phone extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(
            'hotel_phone',
            [
                'id' => $this->primaryKey(),
                'hotel_id' => $this->integer()->notNull(),
                'phone' => $this->string()->notNull(),
            ],
            'ENGINE=InnoDB DEFAULT CHARSET=utf8mb4'
        );
        $this->addForeignKey('fk_hotel_phone_hotel_id', 'hotel_phone', 'hotel_id', 'hotel', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('hotel_phone');
    }
}
