<?php

use yii\db\Migration;

/**
 * Class m200729_183044_create_table_upload
 */
class m200729_183044_create_table_upload extends Migration
{
    public function safeUp()
    {
        $this->createTable(
            'upload',
            [
                'id' => $this->primaryKey(),
                'storage' => $this->tinyInteger()->notNull(),
                'path' => $this->string()->notNull(),
                'type' => $this->string()->notNull(),
                'size' => $this->integer()->notNull(),
                'created_at' => $this->dateTime()->notNull(),
                'created_ip' => $this->integer()->unsigned(),
                'created_by' => $this->integer(),
                'updated_at' => $this->dateTime()->notNull(),
                'updated_ip' => $this->integer()->unsigned(),
                'updated_by' => $this->integer(),
            ],
            'ENGINE=InnoDB DEFAULT CHARSET=utf8mb4'
        );

        $this->addForeignKey('fk_upload_created_by', 'upload', 'created_by', 'user', 'id');
        $this->addForeignKey('fk_upload_updated_by', 'upload', 'updated_by', 'user', 'id');
    }

    public function safeDown()
    {
        $this->dropTable('upload');
    }
}
