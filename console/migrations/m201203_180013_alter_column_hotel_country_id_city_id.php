<?php

use yii\db\Migration;

/**
 * Class m201203_180013_alter_column_hotel_country_id_city_id
 */
class m201203_180013_alter_column_hotel_country_id_city_id extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('hotel', 'country_id', $this->integer());
        $this->alterColumn('hotel', 'city_id', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('hotel', 'country_id', $this->integer()->notNull());
        $this->alterColumn('hotel', 'city_id', $this->integer()->notNull());
    }
}
