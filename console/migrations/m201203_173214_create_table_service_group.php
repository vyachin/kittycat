<?php

use yii\db\Migration;

/**
 * Class m201203_173214_create_table_service_group
 */
class m201203_173214_create_table_service_group extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(
            'service_group',
            [
                'id' => $this->primaryKey(),
                'slug' => $this->string()->notNull()->unique(),
                'name' => $this->string()->notNull(),
                'status' => $this->tinyInteger()->notNull(),
                'created_at' => $this->dateTime()->notNull(),
                'created_ip' => $this->integer()->unsigned(),
                'created_by' => $this->integer(),
                'updated_at' => $this->dateTime()->notNull(),
                'updated_ip' => $this->integer()->unsigned(),
                'updated_by' => $this->integer(),
            ],
            'ENGINE=InnoDB DEFAULT CHARSET=utf8mb4'
        );

        $this->addForeignKey('fk_service_group_created_by', 'service_group', 'created_by', 'user', 'id');
        $this->addForeignKey('fk_service_group_updated_by', 'service_group', 'updated_by', 'user', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('service_group');
    }
}
