<?php

use yii\db\Migration;

/**
 * Class m201203_172454_add_column_room_max_pets
 */
class m201203_172454_add_column_room_max_pets extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('room', 'max_pets', $this->tinyInteger()->notNull()->after('price'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('room', 'max_pets');
    }
}
