<?php

use yii\db\Migration;

/**
 * Class m201202_194107_alter_column_hotel_lat_lng
 */
class m201202_194107_alter_column_hotel_lat_lng extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('hotel', 'lat', $this->double());
        $this->alterColumn('hotel', 'lng', $this->double());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('hotel', 'lat', $this->double()->notNull());
        $this->alterColumn('hotel', 'lng', $this->double()->notNull());
    }
}
