<?php

use yii\db\Migration;

/**
 * Class m200920_070153_create_table_hotel_image
 */
class m200920_070153_create_table_hotel_image extends Migration
{
    public function safeUp()
    {
        $this->createTable(
            'hotel_image',
            [
                'id' => $this->primaryKey(),
                'hotel_id' => $this->integer()->notNull(),
                'image_id' => $this->integer()->notNull(),
                'status' => $this->tinyInteger()->notNull(),
                'created_at' => $this->dateTime()->notNull(),
                'created_by' => $this->integer()->notNull(),
                'created_ip' => $this->integer()->unsigned()->notNull(),
                'updated_at' => $this->dateTime()->notNull(),
                'updated_by' => $this->integer()->notNull(),
                'updated_ip' => $this->integer()->unsigned()->notNull(),
            ],
            'ENGINE=InnoDB DEFAULT CHARSET=utf8mb4'
        );

        $this->addForeignKey('fk_hotel_image_hotel_id', 'hotel_image', 'hotel_id', 'hotel', 'id');
        $this->addForeignKey('fk_hotel_image_image_id', 'hotel_image', 'image_id', 'upload', 'id');
        $this->addForeignKey('fk_hotel_image_created_by', 'hotel_image', 'created_by', 'user', 'id');
        $this->addForeignKey('fk_hotel_image_updated_by', 'hotel_image', 'updated_by', 'user', 'id');
    }

    public function safeDown()
    {
        $this->dropTable('hotel_image');
    }
}
