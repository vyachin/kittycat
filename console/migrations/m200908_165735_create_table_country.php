<?php

use yii\db\Migration;

/**
 * Class m200908_165735_create_table_country
 */
class m200908_165735_create_table_country extends Migration
{
    public function safeUp()
    {
        $this->createTable(
            'country',
            [
                'id' => $this->primaryKey(),
                'slug' => $this->string()->notNull()->unique(),
                'name' => $this->string()->notNull(),
                'status' => $this->tinyInteger()->notNull(),
                'created_at' => $this->dateTime()->notNull(),
                'created_ip' => $this->integer()->unsigned(),
                'created_by' => $this->integer(),
                'updated_at' => $this->dateTime()->notNull(),
                'updated_ip' => $this->integer()->unsigned(),
                'updated_by' => $this->integer(),
            ],
            'ENGINE=InnoDB DEFAULT CHARSET=utf8mb4'
        );

        $this->addForeignKey('fk_country_created_by', 'country', 'created_by', 'user', 'id');
        $this->addForeignKey('fk_country_updated_by', 'country', 'updated_by', 'user', 'id');
    }

    public function safeDown()
    {
        $this->dropTable('country');
    }
}
