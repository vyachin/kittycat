<?php

use yii\db\Migration;

/**
 * Class m201203_183304_add_column_room_image_id
 */
class m201203_183304_add_column_room_image_id extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('room', 'image_id', $this->integer()->after('price'));
        $this->addForeignKey('fk_room_image_id', 'room', 'image_id', 'upload', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_room_image_id', 'room');
        $this->dropColumn('room', 'image_id');
    }
}
