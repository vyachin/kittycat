<?php

use yii\db\Migration;

/**
 * Class m201203_163058_alter_table_country
 */
class m201203_163058_alter_table_country extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('country', 'code', $this->char(2)->after('id')->unique());
        $this->dropColumn('country', 'slug');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('country', 'code');
        $this->addColumn('country', 'slug', $this->string()->notNull()->unique());
    }
}
