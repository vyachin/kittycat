<?php

use yii\db\Migration;

/**
 * Class m201202_193703_alter_column_hotel_email
 */
class m201202_193703_alter_column_hotel_email extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('hotel', 'email', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('hotel', 'email', $this->string()->notNull());
    }
}
