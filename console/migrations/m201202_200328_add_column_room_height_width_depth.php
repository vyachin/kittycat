<?php

use yii\db\Migration;

/**
 * Class m201202_200328_add_column_room_height_width_depth
 */
class m201202_200328_add_column_room_height_width_depth extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('room', 'height', $this->integer()->after('price'));
        $this->addColumn('room', 'width', $this->integer()->after('height'));
        $this->addColumn('room', 'depth', $this->integer()->after('width'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('room', 'height');
        $this->dropColumn('room', 'width');
        $this->dropColumn('room', 'depth');
    }
}
