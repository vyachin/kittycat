<?php

use yii\db\Migration;

/**
 * Class m200908_195636_create_table_service
 */
class m200908_195636_create_table_service extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(
            'service',
            [
                'id' => $this->primaryKey(),
                'slug' => $this->string()->notNull()->unique(),
                'name' => $this->string()->notNull(),
                'status' => $this->tinyInteger()->notNull(),
                'created_at' => $this->dateTime()->notNull(),
                'created_ip' => $this->integer()->unsigned(),
                'created_by' => $this->integer(),
                'updated_at' => $this->dateTime()->notNull(),
                'updated_ip' => $this->integer()->unsigned(),
                'updated_by' => $this->integer(),
            ],
            'ENGINE=InnoDB DEFAULT CHARSET=utf8mb4'
        );

        $this->addForeignKey('fk_service_created_by', 'service', 'created_by', 'user', 'id');
        $this->addForeignKey('fk_service_updated_by', 'service', 'updated_by', 'user', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('service');
    }
}
