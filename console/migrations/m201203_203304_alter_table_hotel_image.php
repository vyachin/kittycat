<?php

use yii\db\Migration;

/**
 * Class m201203_203304_alter_table_hotel_image
 */
class m201203_203304_alter_table_hotel_image extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropForeignKey('fk_hotel_image_created_by', 'hotel_image');
        $this->dropForeignKey('fk_hotel_image_updated_by', 'hotel_image');

        $this->dropColumn('hotel_image', 'status');
        $this->dropColumn('hotel_image', 'created_at');
        $this->dropColumn('hotel_image', 'created_by');
        $this->dropColumn('hotel_image', 'created_ip');
        $this->dropColumn('hotel_image', 'updated_at');
        $this->dropColumn('hotel_image', 'updated_by');
        $this->dropColumn('hotel_image', 'updated_ip');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('hotel_image', 'status', $this->tinyInteger()->notNull());
        $this->addColumn('hotel_image', 'created_at', $this->dateTime());
        $this->addColumn('hotel_image', 'created_by', $this->integer());
        $this->addColumn('hotel_image', 'created_ip', $this->integer()->unsigned()->notNull());
        $this->addColumn('hotel_image', 'updated_at', $this->dateTime());
        $this->addColumn('hotel_image', 'updated_by', $this->integer());
        $this->addColumn('hotel_image', 'updated_ip', $this->integer()->unsigned()->notNull());

        $this->addForeignKey('fk_hotel_image_created_by', 'hotel_image', 'created_by', 'user', 'id');
        $this->addForeignKey('fk_hotel_image_updated_by', 'hotel_image', 'updated_by', 'user', 'id');
    }

}
