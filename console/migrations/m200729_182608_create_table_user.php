<?php

use yii\db\Migration;

/**
 * Class m200729_182608_create_table_user
 */
class m200729_182608_create_table_user extends Migration
{
    public function safeUp()
    {
        $this->createTable(
            'user',
            [
                'id' => $this->primaryKey(),
                'email' => $this->string()->notNull()->unique(),
                'password_hash' => $this->char(60)->notNull(),
                'auth_key' => $this->char(64)->notNull(),
                'access_token' => $this->char(64)->notNull()->unique(),
                'role' => $this->tinyInteger()->notNull(),
                'status' => $this->tinyInteger()->notNull(),
                'created_at' => $this->dateTime()->notNull(),
                'created_ip' => $this->integer()->unsigned(),
                'updated_at' => $this->dateTime()->notNull(),
                'updated_ip' => $this->integer()->unsigned(),
            ],
            'ENGINE=InnoDB DEFAULT CHARSET=utf8mb4'
        );
    }

    public function safeDown()
    {
        $this->dropTable('user');
    }
}
