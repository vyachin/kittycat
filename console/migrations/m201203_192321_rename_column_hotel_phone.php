<?php

use yii\db\Migration;

/**
 * Class m201203_192321_rename_column_hotel_phone
 */
class m201203_192321_rename_column_hotel_phone extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('hotel', 'phone', $this->string());
        $this->renameColumn('hotel', 'phone', '_phone');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->renameColumn('hotel', '_phone', 'phone');
        $this->alterColumn('hotel', 'phone', $this->string()->notNull());
    }
}
