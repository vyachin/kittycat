<?php

use yii\db\Migration;

/**
 * Class m200908_172823_create_table_hotel
 */
class m200908_172823_create_table_hotel extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(
            'hotel',
            [
                'id' => $this->primaryKey(),
                'country_id' => $this->integer()->notNull(),
                'city_id' => $this->integer()->notNull(),
                'slug' => $this->string()->notNull()->unique(),
                'name' => $this->string()->notNull(),
                'description' => $this->text(),
                'address' => $this->string()->notNull(),
                'phone' => $this->string()->notNull(),
                'email' => $this->string()->notNull(),
                'website' => $this->string()->notNull(),
                'lat' => $this->double()->notNull(),
                'lng' => $this->double()->notNull(),
                'status' => $this->tinyInteger()->notNull(),
                'created_at' => $this->dateTime()->notNull(),
                'created_ip' => $this->integer()->unsigned(),
                'created_by' => $this->integer(),
                'updated_at' => $this->dateTime()->notNull(),
                'updated_ip' => $this->integer()->unsigned(),
                'updated_by' => $this->integer(),
            ],
            'ENGINE=InnoDB DEFAULT CHARSET=utf8mb4'
        );

        $this->addForeignKey('fk_hotel_country_id', 'hotel', 'country_id', 'country', 'id');
        $this->addForeignKey('fk_hotel_city_id', 'hotel', 'city_id', 'city', 'id');
        $this->addForeignKey('fk_hotel_created_by', 'hotel', 'created_by', 'user', 'id');
        $this->addForeignKey('fk_hotel_updated_by', 'hotel', 'updated_by', 'user', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('hotel');
    }
}
