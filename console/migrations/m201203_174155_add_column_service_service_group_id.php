<?php

use yii\db\Migration;

/**
 * Class m201203_174155_add_column_service_service_group_id
 */
class m201203_174155_add_column_service_service_group_id extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('service', 'service_group_id', $this->integer()->after('id'));
        $this->addForeignKey('fk_service_service_group_id', 'service', 'service_group_id', 'service_group', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_service_service_group_id', 'service');
        $this->dropColumn('service', 'service_group_id');
    }
}
