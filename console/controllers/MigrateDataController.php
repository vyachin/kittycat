<?php

namespace console\controllers;

use Yii;
use yii\console\Controller;

class MigrateDataController extends Controller
{
    public function actionIssue3()
    {
        echo Yii::$app->db->createCommand('insert into hotel_phone(hotel_id, phone) select id, _phone from hotel')
            ->execute();
    }
}