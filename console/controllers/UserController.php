<?php

namespace console\controllers;

use console\forms\User;
use yii\console\Controller;
use yii\helpers\VarDumper;

class UserController extends Controller
{
    public function actionCreate($email = null, $password = null)
    {
        $model = new User();
        $model->email = $email ?: $this->prompt('Email: ');
        $model->password = $password ?: $this->prompt('Password: ');
        if ($model->save()) {
            echo "Success\n";
        } else {
            $errors = VarDumper::dumpAsString($model->errors);
            echo "Error\n{$errors}\n";
        }
    }
}