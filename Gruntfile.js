const sass = require("node-sass");
const tilde_importer = require("grunt-sass-tilde-importer");
const babel = require("@rollup/plugin-babel");
const resolve = require("@rollup/plugin-node-resolve");
const nodeGlobals = require("rollup-plugin-node-globals");
const commonjs = require("@rollup/plugin-commonjs");
const uglify = require("rollup-plugin-uglify");
const svelte = require("rollup-plugin-svelte");
const env = process.env.NODE_ENV || "development";

module.exports = function (grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON("package.json"),
        copy: {
            backend: {
                files: [
                    {
                        expand: true,
                        cwd: "node_modules/@fortawesome/fontawesome-free/webfonts/",
                        src: "**",
                        dest: "static/fonts/",
                    },
                    {
                        expand: true,
                        cwd: 'backend/assets/img/',
                        src: ['no_image.png'],
                        dest: "static/img/",
                    },
                ]
            },
            frontend: {
                files: [
                    {
                        expand: true,
                        cwd: 'frontend/assets/img/',
                        src: ['under-construction.png'],
                        dest: "static/img/",
                    },
                ]
            }
        },
        sass: {
            options: {
                implementation: sass,
                sourceMap: true,
                importer: tilde_importer
            },
            backend: {
                files: {
                    "static/css/backend.css": "backend/assets/scss/style.scss",
                }
            },
        },
        rollup: {
            options: {
                format: "iife",
                external: [
                    "jquery"
                ],
                globals: {
                    jquery: "$",
                },
                plugins: [
                    resolve.default(),
                    svelte(),
                    babel.default({
                        babelrc: false,
                        babelHelpers: 'bundled',
                        presets: [
                            [
                                "@babel/preset-env",
                                {
                                    "targets": "cover 99.9%",
                                    "loose": true,
                                    "modules": false,
                                    "useBuiltIns": false,
                                }
                            ],
                        ],
                        plugins: [
                            "@babel/plugin-proposal-class-properties",
                        ],
                    }),
                    nodeGlobals(),
                    commonjs({
                        include: [],
                    }),
                    env !== 'development' ? uglify.uglify() : null,
                ],
            },
            backend: {
                options: {
                    name: "KK",
                    sourcemap: true
                },
                files: {
                    "runtime/backend.js": "backend/assets/js/script.js"
                }
            }
        },
        concat: {
            backend: {
                dest: "static/js/backend.js",
                src: [
                    "node_modules/jquery/dist/jquery.min.js",
                    "vendor/yiisoft/yii2/assets/yii.js",
                    "vendor/yiisoft/yii2/assets/yii.validation.js",
                    "vendor/yiisoft/yii2/assets/yii.activeForm.js",
                    "vendor/yiisoft/yii2/assets/yii.gridView.js",
                    "runtime/backend.js",
                ]
            },
        },
        watch: {
            options: {livereload: false, spawn: false},
            styles: {
                files: ["./backend/assets/scss/**/*.scss"],
                tasks: ["sass"],
            },
            script: {
                files: ["./backend/assets/js/**/*"],
                tasks: ["rollup", "concat"],
            },
            file: {
                files: ["./backend/assets/img/**/*"],
                tasks: ["copy"],
            }
        },
    });

    grunt.loadNpmTasks("grunt-sass");
    grunt.loadNpmTasks("grunt-rollup");
    grunt.loadNpmTasks("grunt-contrib-concat");
    grunt.loadNpmTasks("grunt-contrib-copy");
    grunt.loadNpmTasks("grunt-contrib-watch");

    grunt.registerTask("default", ["copy", "sass", "rollup", "concat"]);
};
