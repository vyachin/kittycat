<?php

namespace common\widgets;

use yii\bootstrap4\Html;
use yii\bootstrap4\InputWidget;

class Select extends InputWidget
{
    public $items;

    public function init()
    {
        parent::init();
        Html::addCssClass($this->options, 'selectpicker');
    }

    public function run()
    {
        if ($this->hasModel()) {
            return Html::activeDropDownList($this->model, $this->attribute, $this->items, $this->options);
        }
        return Html::dropDownList($this->name, $this->value, $this->items, $this->options);
    }
}
