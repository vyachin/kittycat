<?php

namespace {

    use common\components\FileStorageComponent;
    use common\components\Formatter;
    use common\components\GoogleGeoCoder;
    use yii\BaseYii;
    use yii\web\User;

    require __DIR__ . '/../vendor/yiisoft/yii2/BaseYii.php';

    /**
     * @property-read Formatter $formatter
     * @property-read WebUser $user
     * @property-read FileStorageComponent $fileStorage
     * @property-read GoogleGeoCoder $geocoder
     */
    trait BaseApplication
    {
    }

    class Yii extends BaseYii
    {
        /**
         * @var WebApplication|ConsoleApplication the application instance
         */
        public static $app;
    }

    /**
     * Class WebUser
     *
     * @property-read \common\models\User $identity
     */
    class WebUser extends User
    {
    }

    class WebApplication extends yii\web\Application
    {
        use BaseApplication;
    }

    class ConsoleApplication extends yii\console\Application
    {
        use BaseApplication;
    }
}
