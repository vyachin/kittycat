<?php

namespace common\helpers;

use common\models\Upload;
use Yii;
use yii\base\Exception;

abstract class Image
{
    public const NO_IMAGE = '/static/img/no_image.png';

    public static function resize($storage, $path, $width, $height): string
    {
        $width = $width ?: '-';
        $height = $height ?: '-';
        if ($storage == Upload::STORAGE_LOCAL) {
            $path = "r/{$width}/{$height}/$path";
            $hash = md5($path . Yii::$app->params['image_secret']);
            return "/il/{$hash}/{$path}";
        }

        throw new Exception("Error storage id {$storage}");
    }

    public static function crop($storage, $path, $width, $height): string
    {
        $width = $width ?: '-';
        $height = $height ?: '-';
        if ($storage == Upload::STORAGE_LOCAL) {
            $path = "c/{$width}/{$height}/$path";
            $hash = md5($path . Yii::$app->params['image_secret']);
            return "/il/{$hash}/{$path}";
        }

        throw new Exception("Error storage id {$storage}");
    }
}