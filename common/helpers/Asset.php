<?php

namespace common\helpers;

use Yii;
use yii\helpers\Url;

abstract class Asset
{
    public static function staticUrl($path)
    {
        $v = @filemtime(Yii::getAlias("@static/{$path}"));

        return Url::to("@staticUrl/{$path}?_={$v}");
    }
}
