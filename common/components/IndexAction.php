<?php

namespace common\components;

use Yii;
use yii\base\Action;
use yii\base\Model;
use yii\di\Instance;

class IndexAction extends Action
{
    public $modelClass;
    public $scenario;
    public $view = 'index';

    public function run()
    {
        /** @var SearchModelInterface|Model $searchModel */
        $searchModel = Instance::ensure($this->modelClass, SearchModelInterface::class);
        if (is_callable($this->scenario)) {
            $scenario = call_user_func($this->scenario);
        } else {
            $scenario = $this->scenario;
        }
        if ($scenario) {
            $searchModel->setScenario($scenario);
        }

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->controller->render(
            $this->view,
            [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]
        );
    }
}
