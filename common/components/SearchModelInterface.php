<?php

namespace common\components;

use yii\data\ActiveDataProvider;

interface SearchModelInterface
{
    public function search($params = []): ActiveDataProvider;
}