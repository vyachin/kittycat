<?php

namespace common\components;

use Yii;
use yii\base\Component;
use yii\base\Exception;
use yii\helpers\FileHelper;

class LocalFileStorage extends Component implements FileStorageInterface
{
    public $basePath;
    public $baseUrl;

    public function init()
    {
        parent::init();
        $this->basePath = realpath(Yii::getAlias($this->basePath));
        $this->baseUrl = Yii::getAlias($this->baseUrl);
    }

    /**
     * @param string $sourceFilePath
     * @param string $fileName
     * @param string $mimeType
     * @throws Exception
     */
    public function upload(string $sourceFilePath, string $fileName, string $mimeType)
    {
        $fullPath = $this->basePath . '/' . $fileName;
        FileHelper::createDirectory(dirname($fullPath));
        copy($sourceFilePath, $fullPath);
    }

    public function url(string $path): string
    {
        return $this->baseUrl . '/' . $path;
    }
}