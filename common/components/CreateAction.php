<?php

namespace common\components;

use Yii;
use yii\base\Action;
use yii\base\Exception;
use yii\db\ActiveRecord;
use yii\di\Instance;

class CreateAction extends Action
{
    public $modelClass;
    public $scenario;
    public $view = '_form';
    public $dependencies = [];

    public function run()
    {
        /** @var ActiveRecord $model */
        $model = Instance::ensure($this->modelClass);

        if (is_callable($this->scenario)) {
            $scenario = call_user_func($this->scenario);
        } else {
            $scenario = $this->scenario;
        }
        if ($scenario) {
            $model->setScenario($scenario);
        }

        if ($this->dependencies) {
            $intersect = array_intersect_key(Yii::$app->getRequest()->get(), array_flip($this->dependencies));
            if (count($intersect) != count($this->dependencies)) {
                throw new Exception('Указаны не все обязательные параметры');
            }

            $model->setAttributes($intersect);
        }

        if ($model->load(Yii::$app->getRequest()->post()) && $model->save()) {
            return $this->controller->asJson(['model' => $model]);
        }

        $form = $this->controller->renderPartial($this->view, ['model' => $model]);
        return $this->controller->asJson(['form' => $form]);
    }
}
