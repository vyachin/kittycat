<?php

namespace common\components;

use yii\base\Component;
use yii\base\Exception;
use yii\base\InvalidConfigException;
use yii\di\Instance;

class FileStorageComponent extends Component
{
    public $items = [];

    /**
     * @throws InvalidConfigException
     */
    public function init()
    {
        parent::init();
        $result = [];
        foreach ($this->items as $index => $reference) {
            $result[$index] = Instance::ensure($reference, FileStorageInterface::class);
        }
        $this->items = $result;
    }

    public function get($index): FileStorageInterface
    {
        if (empty($this->items[$index])) {
            throw new Exception("Storage '$index' not found");
        }
        return $this->items[$index];
    }
}