<?php

namespace common\components;

use libphonenumber\NumberParseException;
use libphonenumber\PhoneNumberFormat;
use libphonenumber\PhoneNumberUtil;
use Yii;
use yii\validators\Validator;

class PhoneValidator extends Validator
{
    public $defaultRegion;

    public function init()
    {
        parent::init();
        if ($this->message === null) {
            $this->message = Yii::t('app', 'Invalid phone number');
        }
        if ($this->defaultRegion === null && preg_match('#^\w+-(\w+)$#', Yii::$app->language, $match)) {
            $this->defaultRegion = $match[1];
        }
    }

    public function validateAttribute($model, $attribute)
    {
        $value = $model->$attribute;
        if ($this->isEmpty($value)) {
            return;
        }

        $instance = PhoneNumberUtil::getInstance();
        try {
            if (!is_string($value)) {
                throw new NumberParseException(NumberParseException::NOT_A_NUMBER, 'The phone number is not string.');
            }
            $phone = $instance->parse($value, $this->defaultRegion);
            if (!$phone || !$instance->isValidNumber($phone) || !$instance->isPossibleNumber($phone)) {
                throw new NumberParseException(NumberParseException::NOT_A_NUMBER, 'The phone number is invalid.');
            }
            $model->$attribute = $instance->format($phone, PhoneNumberFormat::E164);
        } catch (NumberParseException $e) {
            $this->addError($model, $attribute, $this->message, []);
        }
    }
}