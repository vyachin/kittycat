<?php

namespace common\components;

use yii\base\InvalidConfigException;
use yii\base\Request;
use yii\behaviors\AttributeBehavior;
use yii\db\ActiveRecord;
use yii\di\Instance;

class IpBehaviour extends AttributeBehavior
{
    /** @var string|Request */
    public $request = 'request';

    public $attributes = [
        ActiveRecord::EVENT_BEFORE_INSERT => ['created_ip', 'updated_ip'],
        ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_ip',
    ];

    /**
     * @throws InvalidConfigException
     */
    public function init(): void
    {
        parent::init();
        $this->request = Instance::ensure($this->request, Request::class);
    }

    public function getValue($event)
    {
        if (method_exists($this->request, 'getUserIP')) {
            return ip2long($this->request->getUserIP());
        }

        return null;
    }
}
