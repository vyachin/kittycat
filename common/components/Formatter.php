<?php

namespace common\components;

use common\models\City;
use common\models\Country;
use common\models\Hotel;
use common\models\Room;
use common\models\Service;
use common\models\ServiceGroup;

class Formatter extends \yii\i18n\Formatter
{
    public function asCountryStatus($value)
    {
        return Country::getStatusList()[$value] ?? null;
    }

    public function asCityStatus($value)
    {
        return City::getStatusList()[$value] ?? null;
    }

    public function asHotelStatus($value)
    {
        return Hotel::getStatusList()[$value] ?? null;
    }

    public function asServiceStatus($value)
    {
        return Service::getStatusList()[$value] ?? null;
    }

    public function asServiceGroupStatus($value)
    {
        return ServiceGroup::getStatusList()[$value] ?? null;
    }

    public function asRoomStatus($value)
    {
        return Room::getStatusList()[$value] ?? null;
    }
}