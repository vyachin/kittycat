<?php

namespace common\components;

class Coordinate
{
    public $lat;
    public $lng;
    public $countryCode;
    public $cityName;
}