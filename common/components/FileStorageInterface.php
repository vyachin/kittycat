<?php

namespace common\components;

interface FileStorageInterface
{
    public function upload(string $sourceFilePath, string $fileName, string $mimeType);

    public function url(string $path): string;
}