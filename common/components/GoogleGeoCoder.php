<?php

namespace common\components;

use Yii;
use yii\base\Component;
use yii\base\Exception;
use yii\httpclient\Client;

class GoogleGeoCoder extends Component
{
    public $apiKey;

    public function getCoordinate($address): Coordinate
    {
        $data = $this->getResult($address);
        if ($data['status'] == 'OK') {
            $coordinate = new Coordinate();
            $coordinate->lat = (float)$data['results'][0]['geometry']['location']['lat'];
            $coordinate->lng = (float)$data['results'][0]['geometry']['location']['lng'];
            foreach ($data['results'][0]['address_components'] as $addressComponent) {
                if (in_array('country', $addressComponent['types'])) {
                    $coordinate->countryCode = $addressComponent['short_name'];
                } elseif (in_array('locality', $addressComponent['types'])) {
                    $coordinate->cityName = $addressComponent['short_name'];
                }
            }

            return $coordinate;
        }
        throw new Exception('Invalid response');
    }

    protected function getResult($address)
    {
        return Yii::$app->cache->getOrSet(
            [__CLASS__, $address],
            function () use ($address) {
                $client = new Client();
                $request = $client->get(
                    'https://maps.googleapis.com/maps/api/geocode/json',
                    [
                        'address' => $address,
                        'key' => $this->apiKey,
                        'language' => substr(Yii::$app->language, 0, 2),
                    ]
                );
                $response = $client->send($request);
                if ($response->isOk) {
                    Yii::info([(string)$request, (string)$response], self::class);
                    return $response->getData();
                }
                Yii::error([(string)$request, (string)$response], self::class);
                throw new Exception('Error ' . $response->getStatusCode());
            }
        );
    }
}