<?php

namespace common\components;

use Yii;
use yii\base\Action;
use yii\base\Exception;
use yii\db\ActiveRecord;
use yii\di\Instance;
use yii\web\NotFoundHttpException;

class UpdateAction extends Action
{
    public $modelClass;
    public $scenario;
    public $view = '_form';
    public $dependencies = [];

    public function run(array $id)
    {
        /** @var ActiveRecord $model */
        $modelClass = Instance::ensure($this->modelClass);
        $model = $modelClass::findOne($id);
        if (!$model) {
            throw new NotFoundHttpException();
        }
        if (is_callable($this->scenario)) {
            $scenario = call_user_func_array($this->scenario, [$model]);
        } else {
            $scenario = $this->scenario;
        }

        if ($scenario) {
            $model->setScenario($scenario);
        }
        if ($this->dependencies) {
            $intersect = array_intersect_key(Yii::$app->getRequest()->get(), array_flip($this->dependencies));
            if (count($intersect) != count($this->dependencies)) {
                throw new Exception('Указаны не все обязательные параметры');
            }

            $model->setAttributes($intersect);
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->controller->asJson(['model' => $model]);
        }
        $form = $this->controller->renderPartial($this->view, ['model' => $model]);
        return $this->controller->asJson(['form' => $form]);
    }
}
