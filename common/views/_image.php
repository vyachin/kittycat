<?php
/**
 * @var View $this
 * @var Upload $model
 */

use common\models\Upload;
use yii\web\View;

$url = $model ? $model->getUrl() : Yii::getAlias('@staticUrl/img/no_image.png');
?>
<div><img class="img-thumbnail" src="<?= $url ?>" alt=""></div>
<button type="button" class="btn btn-success js-upload">Загрузить</button>
<button type="button" class="btn btn-danger js-clean">Удалить</button>
