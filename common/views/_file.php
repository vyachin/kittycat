<?php
/**
 * @var View $this
 * @var Upload $model
 */

use common\models\Upload;
use yii\bootstrap4\Html;
use yii\web\View;

?>
<div class="input-group mb-3">
    <div class="js-file-name form-control input-sm"><?= $model ? Html::encode($model->getFileName()) : null ?></div>
    <div class="input-group-append">
        <button type="button" class="btn btn-outline-secondary">Загрузить</button>
    </div>
</div>

