<?php

return [
    'bootstrap' => ['log', 'queue'],
    'runtimePath' => __DIR__ . '/../../runtime',
    'language' => 'ru-RU',
    'extensions' => require(__DIR__ . '/../../vendor/yiisoft/extensions.php'),
    'aliases' => [
        'bower' => __DIR__ . '/../../vendor/bower-asset',
        'npm' => __DIR__ . '/../../vendor/npm-asset',
    ],
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'charset' => 'utf8',
            'enableSchemaCache' => !YII_DEBUG,
            'enableProfiling' => YII_DEBUG,
            'enableLogging' => YII_DEBUG,
        ],
        'redis' => [
            'class' => 'yii\redis\Connection',
            'hostname' => 'redis',
        ],
        'cache' => [
            'class' => 'yii\redis\Cache',
            'redis' => 'redis',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
        ],
        'queue' => [
            'class' => 'yii\queue\redis\Queue',
            'redis' => 'redis',
        ],
        'formatter' => [
            'class' => 'common\components\Formatter',
        ],
        'geocoder' => [
            'class' => 'common\components\GoogleGeoCoder',
        ],
        'fileStorage' => [
            'class' => 'common\components\FileStorageComponent',
            'items' => [
                \common\models\Upload::STORAGE_LOCAL => [
                    'class' => 'common\components\LocalFileStorage',
                    'basePath' => '@storage',
                    'baseUrl' => '@storageUrl',
                ],
            ],
        ],
        'log' => [
            'targets' => [
                'geocoder' => [
                    'class' => 'yii\log\FileTarget',
                    'logFile' => '@runtime/logs/geocoder.log',
                    'categories' => [
                        \common\components\GoogleGeoCoder::class,
                    ],
                ],
            ],
        ],
    ],
    'params' => [
        'image_secret' => 'secret',
        'googleMapsApiKey' => '',
    ],
];