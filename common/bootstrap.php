<?php

defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'dev');

require(__DIR__ . '/../vendor/autoload.php');
require(__DIR__ . '/../vendor/yiisoft/yii2/Yii.php');

Yii::setAlias('common', __DIR__);
Yii::setAlias('console', __DIR__ . '/../console');
Yii::setAlias('backend', __DIR__ . '/../backend');
Yii::setAlias('storage', __DIR__ . '/../storage');
Yii::setAlias('storageUrl', '/storage');
Yii::setAlias('static', __DIR__ . '/../static');
Yii::setAlias('staticUrl', '/static');