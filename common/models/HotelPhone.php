<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * Class HotelPhone
 * @package common\models
 * @property int $id
 * @property int $hotel_id
 * @property string $phone
 * @property-read Hotel $hotel
 */
class HotelPhone extends ActiveRecord
{
    public static function tableName(): string
    {
        return 'hotel_phone';
    }

    public function attributeLabels(): array
    {
        return [
            'hotel_id' => Yii::t('app', 'Hotel'),
            'phone' => Yii::t('app', 'Phone'),
        ];
    }

    public function getHotel()
    {
        return $this->hasOne(Hotel::class, ['id' => 'hotel_id']);
    }
}