<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * Class HotelImage
 * @package common\models
 * @property int $id
 * @property int $hotel_id
 * @property int $image_id
 * @property int $status
 * @property string $created_at
 * @property string $updated_at
 * @property int $created_ip
 * @property int $updated_ip
 * @property int $created_by
 * @property int $updated_by
 *
 * @property-read Hotel $hotel
 * @property-read Upload $image
 */
class HotelImage extends ActiveRecord
{
    public static function tableName(): string
    {
        return 'hotel_image';
    }

    public function attributeLabels(): array
    {
        return [
            'hotel_id' => Yii::t('app', 'Hotel'),
            'image_id' => Yii::t('app', 'Image'),
        ];
    }

    public function getHotel()
    {
        return $this->hasOne(Hotel::class, ['id' => 'hotel_id']);
    }

    public function getImage()
    {
        return $this->hasOne(Upload::class, ['id' => 'image_id']);
    }
}