<?php

namespace common\models;

use common\components\IpBehaviour;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * Class Room
 * @package common\models
 * @property int $id
 * @property int $hotel_id
 * @property string $name
 * @property string $description
 * @property double $price
 * @property int $max_pets
 * @property int $height
 * @property int $width
 * @property int $depth
 * @property int $status
 * @property int $image_id
 * @property string $created_at
 * @property string $updated_at
 * @property int $created_ip
 * @property int $updated_ip
 * @property int $created_by
 * @property int $updated_by
 * @property-read Hotel $hotel
 * @property-read RoomService[] $roomServices
 * @property-read Upload $image
 */
class Room extends ActiveRecord
{
    public const STATUS_ACTIVE = 1;
    public const STATUS_DELETED = 2;

    public static function getStatusList(): array
    {
        return [
            self::STATUS_ACTIVE => Yii::t('app', 'Active'),
            self::STATUS_DELETED => Yii::t('app', 'Deleted'),
        ];
    }

    public static function tableName(): string
    {
        return 'room';
    }

    public function attributeLabels(): array
    {
        return [
            'hotel_id' => Yii::t('app', 'Hotel'),
            'image_id' => Yii::t('app', 'Image'),
            'name' => Yii::t('app', 'Name'),
            'description' => Yii::t('app', 'Description'),
            'status' => Yii::t('app', 'Status'),
            'max_pets' => Yii::t('app', 'Max pets in room'),
            'height' => Yii::t('app', 'Height'),
            'width' => Yii::t('app', 'Width'),
            'depth' => Yii::t('app', 'Depth'),
        ];
    }

    public function behaviors(): array
    {
        return [
            TimestampBehavior::class => [
                'class' => TimestampBehavior::class,
                'value' => function () {
                    return date('Y-m-d H:i:s');
                }
            ],
            IpBehaviour::class => [
                'class' => IpBehaviour::class,
            ],
            BlameableBehavior::class => [
                'class' => BlameableBehavior::class,
            ],
        ];
    }

    public function getHotel()
    {
        return $this->hasOne(Hotel::class, ['id' => 'hotel_id']);
    }

    public function getImage()
    {
        return $this->hasOne(Upload::class, ['id' => 'image_id']);
    }

    public function getRoomServices()
    {
        return $this->hasMany(RoomService::class, ['room_id' => 'id']);
    }
}