<?php

namespace common\models;

use common\components\IpBehaviour;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * Class RoomService
 * @package common\models
 * @property int $id
 * @property int $room_id
 * @property int $service_id
 * @property double $price
 * @property string $created_at
 * @property int $created_ip
 * @property int $created_by
 * @property-read Room $room
 * @property-read Service $service
 */
class RoomService extends ActiveRecord
{
    public static function tableName(): string
    {
        return 'room_service';
    }

    public function attributeLabels(): array
    {
        return [
            'room_id' => Yii::t('app', 'Room'),
            'service_id' => Yii::t('app', 'Service'),
            'price' => Yii::t('app', 'Price'),
        ];
    }

    public function behaviors(): array
    {
        return [
            TimestampBehavior::class => [
                'class' => TimestampBehavior::class,
                'updatedAtAttribute' => false,
                'value' => function () {
                    return date('Y-m-d H:i:s');
                }
            ],
            IpBehaviour::class => [
                'class' => IpBehaviour::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_ip'],
                ]
            ],
            BlameableBehavior::class => [
                'class' => BlameableBehavior::class,
                'updatedByAttribute' => false,
            ],
        ];
    }

    public function getRoom()
    {
        return $this->hasOne(Room::class, ['id' => 'room_id']);
    }

    public function getService()
    {
        return $this->hasOne(Service::class, ['id' => 'service_id']);
    }
}