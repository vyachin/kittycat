<?php

namespace common\models;

use common\components\IpBehaviour;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * Class Hotel
 * @package common\models
 * @property int $id
 * @property int $country_id
 * @property int $city_id
 * @property string $slug
 * @property string $name
 * @property string $description
 * @property string $address
 * @property string $_phone // deprecated
 * @property string $email
 * @property string $website
 * @property double $lat
 * @property double $lng
 * @property int $status
 * @property string $created_at
 * @property string $updated_at
 * @property int $created_ip
 * @property int $updated_ip
 * @property int $created_by
 * @property int $updated_by
 * @property-read Country $country
 * @property-read City $city
 * @property-read Room[] $rooms
 * @property-read Upload[] $images
 * @property-read HotelPhone[] $hotelPhones
 * @property-read HotelImage[] $hotelImages
 */
class Hotel extends ActiveRecord
{
    public const STATUS_ACTIVE = 1;
    public const STATUS_DELETED = 2;

    public static function getStatusList(): array
    {
        return [
            self::STATUS_ACTIVE => Yii::t('app', 'Active'),
            self::STATUS_DELETED => Yii::t('app', 'Deleted'),
        ];
    }

    public static function tableName(): string
    {
        return 'hotel';
    }

    public function attributeLabels(): array
    {
        return [
            'country_id' => Yii::t('app', 'Country'),
            'city_id' => Yii::t('app', 'City'),
            'name' => Yii::t('app', 'Name'),
            'description' => Yii::t('app', 'Description'),
            'status' => Yii::t('app', 'Status'),
            'address' => Yii::t('app', 'Address'),
            'website' => Yii::t('app', 'Web site'),
            'email' => Yii::t('app', 'Email'),
            'lat' => Yii::t('app', 'Latitude'),
            'lng' => Yii::t('app', 'Longitude'),
        ];
    }

    public function behaviors(): array
    {
        return [
            TimestampBehavior::class => [
                'class' => TimestampBehavior::class,
                'value' => function () {
                    return date('Y-m-d H:i:s');
                }
            ],
            IpBehaviour::class => [
                'class' => IpBehaviour::class,
            ],
            BlameableBehavior::class => [
                'class' => BlameableBehavior::class,
            ],
            SluggableBehavior::class => [
                'class' => SluggableBehavior::class,
                'attribute' => 'name',
                'ensureUnique' => true,
            ],
        ];
    }

    public function getCountry()
    {
        return $this->hasOne(Country::class, ['id' => 'country_id']);
    }

    public function getCity()
    {
        return $this->hasOne(City::class, ['id' => 'city_id']);
    }

    public function getRooms()
    {
        return $this->hasMany(Room::class, ['hotel_id' => 'id']);
    }

    public function getHotelImages()
    {
        return $this->hasMany(HotelImage::class, ['hotel_id' => 'id']);
    }

    public function getImages()
    {
        return $this->hasMany(Upload::class, ['id' => 'image_id'])->via('hotelImages');
    }

    public function getHotelPhones()
    {
        return $this->hasMany(HotelPhone::class, ['hotel_id' => 'id']);
    }
}