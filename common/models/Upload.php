<?php

namespace common\models;

use common\components\IpBehaviour;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * Class Upload
 * @package common\models
 * @property int $id
 * @property int $storage
 * @property string $path
 * @property string $type
 * @property int $size
 * @property string $created_at
 * @property string $updated_at
 * @property int $created_ip
 * @property int $updated_ip
 * @property int $created_by
 * @property int $updated_by
 */
class Upload extends ActiveRecord
{
    public const STORAGE_LOCAL = 1;

    public static function getStorageList(): array
    {
        return [
            self::STORAGE_LOCAL => Yii::t('app', 'Local'),
        ];
    }

    public static function tableName()
    {
        return 'upload';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::class => [
                'class' => TimestampBehavior::class,
                'value' => function () {
                    return date('Y-m-d H:i:s');
                }
            ],
            IpBehaviour::class => [
                'class' => IpBehaviour::class,
            ],
            BlameableBehavior::class => [
                'class' => BlameableBehavior::class,
            ],
        ];
    }

    public function getUrl()
    {
        $storage = Yii::$app->fileStorage->get($this->storage);
        if (!$storage) {
            return null;
        }
        return $storage->url($this->path);
    }

    public function fields()
    {
        return [
            'id',
            'url',
        ];
    }
}