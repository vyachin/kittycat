<?php

namespace common\models;

use common\components\IpBehaviour;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * Class Country
 * @package common\models
 * @property int $id
 * @property string $code
 * @property string $name
 * @property int $status
 * @property string $created_at
 * @property string $updated_at
 * @property int $created_ip
 * @property int $updated_ip
 * @property int $created_by
 * @property int $updated_by
 */
class Country extends ActiveRecord
{
    public const STATUS_ACTIVE = 1;
    public const STATUS_DELETED = 2;

    public static function getStatusList(): array
    {
        return [
            self::STATUS_ACTIVE => Yii::t('app', 'Active'),
            self::STATUS_DELETED => Yii::t('app', 'Deleted'),
        ];
    }

    public static function tableName(): string
    {
        return 'country';
    }

    public function attributeLabels(): array
    {
        return [
            'code' => Yii::t('app', 'Code'),
            'name' => Yii::t('app', 'Name'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    public function behaviors(): array
    {
        return [
            TimestampBehavior::class => [
                'class' => TimestampBehavior::class,
                'value' => function () {
                    return date('Y-m-d H:i:s');
                }
            ],
            IpBehaviour::class => [
                'class' => IpBehaviour::class,
            ],
            BlameableBehavior::class => [
                'class' => BlameableBehavior::class,
            ],
        ];
    }

    public function getCities()
    {
        return $this->hasMany(City::class, ['country_id' => 'id']);
    }
}