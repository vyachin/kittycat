<?php
/**
 * @var View $this
 */

use yii\bootstrap4\Html;
use yii\web\View;

$this->title = 'Under construction';
?>
<?= Html::img('@staticUrl/img/under-construction.png', ['alt' => 'Under construction']) ?>
