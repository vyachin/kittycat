<?php
/**
 * @var View $this
 * @var string $content
 */

use common\helpers\Asset;
use yii\bootstrap4\Html;
use yii\web\View;

$this->registerJsFile(Asset::staticUrl('js/frontend.js'));
$this->registerCssFile(Asset::staticUrl('css/frontend.css'));

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?= Html::encode($this->title) ?></title>
    <?= Html::csrfMetaTags() ?>
    <?php $this->head() ?>
</head>
<body style="background-color:#000; text-align: center;">
<?php $this->beginBody() ?>
<?= $content ?>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

