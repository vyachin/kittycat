<?php

use yii\redis\Session;

return [
    'id' => 'kittycat-frontend',
    'name' => 'Admin',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'assetManager' => [
            'bundles' => false,
        ],
        'errorHandler' => [
            'errorAction' => YII_DEBUG ? null : 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
        ],
        'session' => [
            'class' => Session::class,
            'redis' => 'redis',
        ],
        'log' => [
            'traceLevel' => 3,
            'targets' => [
                'error' => [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                    'logFile' => '@runtime/logs/frontend.error.log',
                ],
            ],
        ],
    ],
];